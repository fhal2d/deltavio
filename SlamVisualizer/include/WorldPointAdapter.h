#pragma once


#include <vector>
#include <Eigen/Core>
#include <Eigen/src/Core/util/ForwardDeclarations.h>


struct WorldPointAdapter {
	virtual ~WorldPointAdapter() = default;
	virtual void pushWorldPoint(const std::vector<Eigen::Vector3f>& v_Point3f){};

};
