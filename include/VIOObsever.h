/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "Observer.h"
#include "DataSet/DataSet.h"
#include "WorldPointAdapter.h"
#include "FrameAdpater.h"

using namespace std;


class VIOObsever : public Observer,public DataSet::ImageListener
					
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef std::shared_ptr<VIOObsever> Ptr;

    VIOObsever();

    ~VIOObsever();
	 void ImageReceived(double timeStamp, const cv::Mat& image)override;

     virtual int trackFrame(FrameData::Ptr dataPtr) = 0;

	 void addWorldPointAdapter(WorldPointAdapter* pWorldPointAdapter) { m_pWorldPointAdapter = pWorldPointAdapter; }
	 void addFrameAdapter(FrameAdapter* pFrameAdapter) { m_pFrameAdapter = pFrameAdapter; }
protected:

	FrameAdapter* m_pFrameAdapter=nullptr;
	WorldPointAdapter* m_pWorldPointAdapter=nullptr;
	
private:
	std::atomic_bool haveBricks2Move;


	void WakeUp2MoveBricks() override;

	bool checkHaveBricks2Move() override{
		return haveBricks2Move;
	}

};
