/**
* This file is part of Delta_VIO.
* CamModel and its derived class is rewrite from https://github.com/uzh-rpg/rpg_vikit
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MSCKF_PINHOLE_H
#define MSCKF_PINHOLE_H

#include <CamModel/CamModel.h>


namespace DeltaVIO
{
	class PinholeModel : public CamModel
	{
		PinholeModel(int width, int height, float fx, float fy, float cx, float cy)
			: CamModel(width, height), fx(fx), fy(fy), cx(cx), cy(cy),
			  tfovx(width / (2 * fx)), tfovy(height / (2 * fy))
		{
		};
	public:
		static PinholeModel* createFromConfig(FileStorage& config)
		{
			Mat K;
			Mat D;
			config["Intrinsic"] >> K;
			auto pK = K.ptr<double>();
			return new PinholeModel(pK[0], pK[1], pK[2], pK[3], pK[4], pK[5]);
		}

		Vector3f imageToCam(const Vector2f& px) override
		{
			return Vector3f((px.x() - cx) / fx, (px.y() - cy) / fy, 1).normalized();
		}

		Vector2f camToImage(const Vector3f& pCam) override
		{
			return Vector2f(pCam.x() / pCam.z() * fx + cx, pCam.y() / pCam.z() * fy + cy);
		}

		Vector2f camToImage(const Vector3f& pCam, Matrix23f& J23) override
		{
			float invZ = 1 / pCam.z();
			float ux = pCam.x() * invZ;
			float uy = pCam.y() * invZ;
			J23 << invZ * fx, 0, -fx * invZ * ux,
				0, invZ * fy, -fy * invZ * uy;
			return Vector2f(ux, uy);
		}


		float focal() override
		{
			return fx;
		}

		bool inView(const Vector3f& pCam) override
		{
			return (pCam.x() < tfovx * pCam.z() && pCam.y() < tfovy * pCam.z());
		}

	private:
		float fx, fy, cx, cy;
		float tfovx, tfovy;
	};
}


#endif //MSCKF_PINHOLE_H
