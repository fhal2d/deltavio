/**
* This file is part of Delta_VIO.
* CamModel and its derived class is rewrite from https://github.com/uzh-rpg/rpg_vikit
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MSCKF_CAMMODEL_H
#define MSCKF_CAMMODEL_H

#include "BasicTypes.h"
#include <string>

namespace DeltaVIO
{
	class CamModel
	{
	public:
		CamModel(int width, int height): width_(width), height_(height)
		{
		};

		static void loadCalibrations();

		static void generateCalibrations(const std::string& path);

		static CamModel* getCamModel()
		{
			return camModel;
		}

		void loadCamImuExtrinsic(const Matrix3f& Rci, const Vector3f& Pic)
		{
			this->m_Rci = Rci;
			this->m_Pic = Pic;
		};

		virtual Vector3f imageToCam(const Vector2f& px) =0;

		virtual Vector2f camToImage(const Vector3f& pCam) =0;

		virtual Vector3f imageToImu(const Vector2f& px)
		{
			return m_Rci.transpose() * imageToCam(px);
		}

		Vector2f imuToImage(const Vector3f& pImu)
		{
			return camToImage(m_Rci * (pImu - m_Pic));
		}

		Vector2f imuToImage(const Vector3f& pImu, Matrix23f& J23)
		{
			Vector2f px = camToImage(m_Rci * (pImu - m_Pic), J23);
			J23 = J23 * m_Rci;
			return px;
		}

		virtual Vector2f camToImage(const Vector3f& pCam, Matrix23f& J23) =0;

		virtual float focal() =0;

		virtual int width() { return width_; }

		virtual int height() { return height_; }

		virtual Matrix3f& getRci() { return m_Rci; }

		virtual Vector3f& getPic() { return m_Pic; }

		virtual int area() { return width_ * height_; }

		virtual bool inView(const Vector2f& px)
		{
			return (px.x() >= 0 && px.x() < width_ && px.y() >= 0 && px.y() < height_);
		};
		virtual bool inView(const Vector3f& pCam) { return false; };
	private:
		int width_, height_;
		Matrix3f m_Rci;
		Vector3f m_Pic;
		float m_fImageNoise;
		static CamModel* camModel;
	};
}

#endif //MSCKF_CAMMODEL_H
