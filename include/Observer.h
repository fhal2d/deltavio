/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>


class Observer
{
public:
	Observer()
	{
		keepMoving.store(true);
	}

	virtual ~Observer()
	{
		stop();
		delete modulesThread;
	}

	void join()
	{
		if (joinable())_join();
	}
	void start()
	{
		if (!m_bRunning)
			_start();
	}

	virtual void stop()
	{
		this->_stop();
	}
	void detach()
	{
		if (modulesThread != nullptr && m_bRunning)
			if (!m_bDetached)
			{
				_detach();
			}
	}

	bool joinable() const
	{
		if (modulesThread != nullptr)
			return modulesThread->joinable();
		return false;
	}



	void wakeUpMovers()
	{
		std::lock_guard<std::mutex> lk(wakeUpMutex);
		wakeUpConditionVariable.notify_one();
	}

	virtual void runThread()
	{
		while (keepMoving)
		{
			//check for wake up request
			{
				std::unique_lock<std::mutex> ul(wakeUpMutex);
				wakeUpConditionVariable.wait(ul, [this]() { return this->checkHaveBricks2Move() | !this->keepMoving; });
			}
			//do something when wake up
			while (checkHaveBricks2Move() & this->keepMoving)
			{
				WakeUp2MoveBricks();
			}
		}
	}


protected:
	std::thread* modulesThread = nullptr;
	std::mutex wakeUpMutex;
	std::mutex serialMutex;
	std::condition_variable wakeUpConditionVariable;
	std::condition_variable serialConditionVariable;
	std::atomic_bool keepMoving;
	bool m_bRunning = false;
	bool m_bDetached = false;

	virtual bool checkHaveBricks2Move() = 0;
	virtual void WakeUp2MoveBricks() = 0;

void waitForBricksTobeMoved()
{
    if (Configuration::SerialRun)
    {
        std::unique_lock<std::mutex> lck(serialMutex);
        serialConditionVariable.wait(lck);
    }
}

void tellThemBricksMoved()
{
    if (Configuration::SerialRun)
    {
        std::unique_lock<std::mutex> ul(serialMutex);
        serialConditionVariable.notify_all();
    }
}

private:

	void _stop()
	{
		{
			keepMoving.store(false);
			std::lock_guard<std::mutex> lk(wakeUpMutex);
			wakeUpConditionVariable.notify_one();
		}
		join();
	}

	void _detach()
	{
		modulesThread->detach();
		m_bDetached = true;
	}

	void _start()
	{
		if (m_bRunning)return;
		keepMoving.store(true);

		modulesThread = new std::thread(
			[&]()
			{
				this->runThread();
			});
		m_bRunning = true;
	}

	void _join()
	{
		modulesThread->join();
		m_bRunning = false;
	}

};
