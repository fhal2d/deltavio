/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "BasicTypes.h"
#include "DataStructure/TrackedFeature.h"

namespace DeltaVIO{
	struct ImuPreintergration;
	struct PointState;
	struct CamState;

	class SquareRootEKFSolver
{
public:

	SquareRootEKFSolver();

	void init(CamState* pCamState, Vector3f* vel);

	void addCamState(CamState* state);

	void propagate(const ImuPreintergration* pImuTerm);

	void marginalize();

	bool MahalanobisTest(PointState* state);

	void computeMsckfConstraint(TrackedFeaturePtr track);

	void addVisualObservation(PointState* state);

	void addVelocityConstraint();

	int stackInformationFactorMatrix();

	void solveAndUpdateStates();

private:
	
	MatrixXf m_StackedMatrix;
	MatrixXf m_infoFactorMatrix;
	MatrixXf m_infoFactorInverseMatrix;
	VectorXf m_residual;
	std::vector<CamState*> m_vCamStates;
	std::vector<PointState*> m_vPointStates;
	Vector3f* m_pVel;
	CamState* m_pNewState = nullptr;
	CamState* m_pLastState = nullptr;

};

}
