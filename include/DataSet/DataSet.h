/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "utils/FrameData.h"
#include <thread>
#include <vector>
#include <fstream>
#include <mutex>
#include <string>

namespace DeltaVIO
{



	class DataSet
	{
	public:
		struct ImageData
		{
			long long m_llTimestamp;
			std::string m_imgFilePath;
		};

		struct ImuData
		{
			long long m_llTimestamp;
			float m_data[6]; 
		};
		typedef std::shared_ptr<DataSet> Ptr;

		class ImageListener
		{
		public:
			virtual void ImageReceived(double timeStamp, const cv::Mat& image) = 0;
		};

		class IMUListener
		{
		public:
			virtual void ImuReceived(float* data, double timestamp) = 0;
		};

		virtual bool start() = 0;
		virtual bool stop() = 0;

		virtual void join()
		{
			if (m_Thread.joinable())
			{
				m_Thread.join();
			}
		}

		void addImageListener(ImageListener* l)
		{
			std::lock_guard<std::mutex> lck(m_ImageMutex);
			
			m_ImageListener.push_back(l);
			
		}

		void addImuListener(IMUListener* l)
		{
			std::lock_guard<std::mutex> lck(m_ImuMutex);
			m_ImuListener.push_back(l);
			
		}

		void clearListener()
		{
			{
				std::lock_guard<std::mutex> lck(m_ImageMutex);
				m_ImageListener.clear();
			}
			{
				
				std::lock_guard<std::mutex> lck(m_ImuMutex);
				m_ImuListener.clear();
			}
		}



	protected:
		virtual void runThread(){};
		std::thread m_Thread;
		std::vector<ImageListener*> m_ImageListener;
		std::vector<IMUListener*> m_ImuListener;
		std::mutex m_ImageMutex;
		std::mutex m_ImuMutex;
	};
};
