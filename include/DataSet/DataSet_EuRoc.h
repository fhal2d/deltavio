/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "DataSet.h"

namespace DeltaVIO
{
	class DataSet_EuRoc : public DataSet
	{
	public:
		DataSet_EuRoc();
		~DataSet_EuRoc();

		bool start() override;
		bool stop() override;


	protected:
		void runThread() override;

	private:
		void _loadIMU();
		void _loadImage();



		std::string m_dataSetDir;
		std::string m_camDir;
		std::string m_imuDir;
		int m_iImageIdx;
		int m_iImuIndex;
		std::vector<ImageData> m_vImages;
		std::vector<ImuData> m_vIMU;
	};
}
