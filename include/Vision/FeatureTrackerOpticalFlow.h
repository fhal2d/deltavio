#pragma once

#include "Vision/FeatureTracker.h"

namespace DeltaVIO {

	
	class FeatureTrackerOpticalFlow : public FeatureTracker
	{
	public:
		FeatureTrackerOpticalFlow(int nMax2Track, int nMaskSize=15);
		void _setMask(int x, int y);

		void _extractMorePoints(std::list<TrackedFeaturePtr>& vTrackedFeatures);
		void _trackPoints(std::list<TrackedFeaturePtr>& vTrackedFeatures);
		void matchNewFrame(std::list<TrackedFeaturePtr>& vTrackedFeatures, cv::Mat& image, Frame* camState) override;

		~FeatureTrackerOpticalFlow();
	private:
		unsigned char* m_pMask = nullptr;
		int m_iFeature;
		int m_nMax2Track;
		int m_nMaskSize;
		int m_nTracked;
		int m_nMaskBufferSize;
		cv::Mat m_image;
		Frame* m_pCamState = nullptr;
		Frame* m_pCamState0 = nullptr;
		cv::Mat lastImage;
		
	};


}
