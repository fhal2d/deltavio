#pragma once
#include "DataStructure/TrackedFeature.h"


namespace DeltaVIO
{


	struct FeaturePoint
	{
		FeaturePoint() {};
		float x() { return px.x(); }
		float y() { return px.y(); }
		Vector2f px;
		cv::KeyPoint m_corner;
		cv::Mat m_desc;
		int m_GridIdx;
		bool m_bIsORB = false;
		TrackedFeature* m_pTrackedFeature = nullptr;
	};

	const constexpr int nMaxNumOfFeaturePool = 7000;

	class FeatureTracker
	{
	public:

		FeatureTracker()
		{
			m_pFeaturePool = new FeaturePoint[nMaxNumOfFeaturePool];
		};
		virtual void matchNewFrame(std::list<TrackedFeaturePtr>& vTrackedFeatures, cv::Mat& image, Frame* camState){};
		~FeatureTracker()
		{
			delete[] m_pFeaturePool;
		}
		FeaturePoint* m_pFeaturePool = nullptr;
	};


}