/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "fast.h"
#include "twoPointRansac.h"
#include "DataStructure/TrackedFeature.h"
#include "ORBextractor.h"
#include "Vision/FeatureTracker.h"

namespace DeltaVIO
{

	class FeatureTrackerDescriptor:public FeatureTracker 
	{
	public:

		float m_DistThresh2 = 36;

	public:
		FeatureTrackerDescriptor();
		~FeatureTrackerDescriptor();
		
		void matchNewFrame(std::list<TrackedFeaturePtr>& vTrackedFeatures, cv::Mat& image, Frame* camState) override;


	private:
		void _setGridAndNeighbors();
		void _extractFeatures();
		void _track();
		void _clearAndRebuildLUT();
		void _detect_fast(cv::Mat&img, std::vector<fast::fast_xy>&pts, int detection_threshold);
		void _matchFeatures(std::vector<TrackedFeaturePtr>& goodTracks, std::vector<TrackedFeaturePtr>& badTracks);
		int  _detectMoreORBFeatures();
		void _SSDMatch(std::vector<TrackedFeaturePtr>& goodTracks);
		void _do2PointRansac();

		void _detectFeatures(bool bInit, int detection_threshold);
		void _setCommonData(std::list<TrackedFeaturePtr>& tracked_features, Mat& cur_image, Frame* cam_state);
		void _Median_Filter(std::vector<TrackedFeaturePtr>& goodTracks, std::vector<TrackedFeaturePtr>& badTracks, bool verbose);
		void _switchFrame();
		void _trackNewFrame();
		
		
		TwoPointRansac* m_pTwoPointRansac;
		ORBextractor* m_pORBExtractor;	
		int m_nCorners;
		std::list<TrackedFeaturePtr>* m_pvTrackedFeatures = nullptr;
		std::vector<std::vector<FeaturePoint*> > m_vvGridPoints;
		std::vector<std::vector<int>> m_vvNeighbor_LUT;
		std::vector<vector<TrackedFeature*>> m_vvGridTrackedFeature;
		std::vector<std::vector<FeaturePoint*> > m_vvGridAllFeatures;
		


		Matrix3f dR;
		cv::Mat img1;
		cv::Mat img0;
		Frame* frame1;
		Frame* frame0;



		int m_nXGrid;
        int m_nYGrid;
        int m_nStepX;
        int m_nStepY;


	};
	
}
