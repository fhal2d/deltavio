/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

constexpr float GRAVITY = 9.81f;

constexpr auto CAM_DIM = 6;
constexpr auto NEW_STATE_DIM = 15;
constexpr auto IMU_DIM = 9;

constexpr auto CAM_DELETE_STEP = 3;
constexpr auto MAX_CAMERA_STATE = 10;
constexpr auto MAX_MSCKF_FEATURE = 20;

constexpr int EDGE_THRESHOLD = 20;


#define _RED_SCALAR cv::Scalar(0,0,255,1)
#define _GREEN_SCALAR cv::Scalar(0,255,0,1)
#define _BLUE_SCALAR cv::Scalar(255,0,0,1)