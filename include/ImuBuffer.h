/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <Eigen/Dense>

#include "DataSet/DataSet.h"
#include "BasicTypes.h"
#include "utils/CircularBuffer.h"

namespace DeltaVIO {
	struct ImuPreintergration;

struct ImuData
{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
	double timestamp;
	Eigen::Vector3f gyro;
	Eigen::Vector3f acc;
	bool operator< (double t)const{return timestamp<t;}
	bool operator==(double t)const{return timestamp==t;}
	bool operator>(double t)const{return timestamp>t;}
	bool operator<=(double t)const{return timestamp<=t;}
	bool operator>=(double t)const{return timestamp>=t;}
};


class ImuBuffer 
	: public CircularBuffer<ImuData, 512>,public DataSet::IMUListener
{

public:
	static ImuBuffer& getBuffer() {
		static ImuBuffer buffer;
		return buffer;
	}

	void ImuReceived(float *data, double timestamp) override;
	bool getDataByBinarySearch(ImuData& imuData) const;

	bool imuPreIntegration(ImuPreintergration& ImuTerm) const;

	void updateBias(const Vector3f& dBg, const Vector3f& dBa);

	void setBias(const Vector3f& bg, const Vector3f& ba);

	void setZeroBias();

	void getBias(Vector3f& bg, Vector3f& ba) const;

	Vector3f getGravity();
private:
	int findDataByBinarySearch(double timestamp) const;

	ImuBuffer();

	MatrixXf m_NoiseCov;

	Vector3f m_GyroBias;
	Vector3f m_AccBias;

	Vector3f mGravity;
	std::mutex m_gMutex;
};

}
