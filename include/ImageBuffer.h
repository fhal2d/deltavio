/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "utils/CircularBuffer.h"
#include "utils/FrameData.h"


namespace DeltaVIO {

class ImageBuffer 
	: public CircularBuffer<FrameData::Ptr, 4>
{

	ImageBuffer();

public:
	static ImageBuffer& getBuffer() {
		static ImageBuffer buffer;
		return buffer;
	}


	bool pushImage(double timestamp, const cv::Mat& image);
	
	FrameData::Ptr popImage();

};

}
