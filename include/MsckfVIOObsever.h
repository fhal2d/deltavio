/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "DataStructure/ImuPreintergration.h"
#include "DataStructure/Frame.h"
#include "utils/FrameData.h"
#include "VIOObsever.h"
#include "SquareRootEKFSolver.h"
#include "SlamVisualizer.h"
#include "Vision/FeatureTrackerOpticalFlow.h"

namespace DeltaVIO {
	class FeatureTrackerDescriptor;
	class MsckfAlgoModule;

	struct MsckfDataPack
	{
		typedef std::shared_ptr<MsckfDataPack> Ptr;

		MsckfDataPack(const ImuPreintergration&imu, cv::Mat&img){
			image = img.clone(); imuPreintegration = imu;
		}
		cv::Mat image;
		ImuPreintergration imuPreintegration;
	};



	class MsckfVIOObsever : public VIOObsever
	{
	public:
		MsckfVIOObsever();
		~MsckfVIOObsever();

		int trackFrame(FrameData::Ptr imageDataPtr) override;

	private:

		void _processInputData(FrameData::Ptr imageDataPtr);
		void _processOutputData(FrameData::Ptr imageDataPtr);
		void _updatePointsAndCamsToVisualizer();
		void _drawTrackImage(FrameData::Ptr dataPtr, cv::Mat& trackImage);

		bool m_bInited = false;
		MsckfDataPack::Ptr m_pDataInputPack;
		ImuPreintergration m_imuTerm;
		std::list<TrackedFeaturePtr> m_lTrackedFeatures;

		FeatureTrackerDescriptor* m_pVisionModule;
		FeatureTrackerOpticalFlow* m_pTrackerOpticalFlow = nullptr;

		void initialize(const Matrix3f& Rwi);

		void addIMUInformation();

		void _removeDeadFeatures();

		void addVisionInformation();


		void _pushPoints2Grid(std::vector<TrackedFeaturePtr>& vDeadFeature);
		void _addBufferPoints(std::vector<TrackedFeaturePtr>& vDeadFeature);
		void _addDeadPoints(std::vector<TrackedFeaturePtr>& vDeadFeature);

		void _selectFrames2Margin();

		void _marginFrames();

		void _selectPointsToUpdate();

		void _tryAddMsckfPoseConstraint();

		void _stackInformationFactorMatrix();

		bool _DetectStill();

		void _computeMsckfPoseConstraint(TrackedFeaturePtr track);

		void _testVisionModule();

	public:

		SquareRootEKFSolver			m_solver;
		MsckfState					m_msckfState;

		FramePtr					m_pFrameNew = nullptr;
		FramePtr					m_pFrameLast = nullptr;

		MatrixXf					m_infoFactor;

		MatrixXf					m_infoFactorInverse;
		VectorXf					m_residual;


		std::vector<TrackedFeaturePtr> m_vTrackedFeatureToUpdate;
		std::vector<TrackedFeaturePtr> m_vTrackedFeatureNextUpdate;
		std::vector<vector<TrackedFeaturePtr>> m_Grid22;


	};

}


