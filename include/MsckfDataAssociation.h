#pragma once

#include "twoPointRansac.h"
#include "DataStructure/TrackedFeature.h"
#include "SquareRootEKFSolver.h"

namespace DeltaVIO
{
	namespace DataAssociation
	{

		void initDataAssociation(SquareRootEKFSolver*solver);

		void removeOutlierBy2PointRansac(Matrix3f& dR, std::list<TrackedFeaturePtr>& vTrackedFeatures);

		void doDataAssociation(std::list<TrackedFeaturePtr>& vTrackedFeatures);
		
	};
	
}
