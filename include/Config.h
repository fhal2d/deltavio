/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <opencv2/opencv.hpp>

using cv::FileStorage;
using std::string;
namespace DeltaVIO
{

    class Configuration {
    public:
        enum DataSouceType {
            EUROC = 0,
            TUM = 1,
            ICL = 2
        };

        static int DatasetType;
        static string DatasetPath;
        static string CameraCalibFile;


        static float ImageNoise2;
        static float GyroNoise2;
        static float AccNoise2;
        static float GyroBiasNoise2;
        static float AccBiasNoise2;

        static int SerialRun;
        static int ImageStartNumber;
        static int nImuSample;

        static void readConfigFile();

    	static void generateTemplateFile();

    private:
        static void _clear();

        static FileStorage m_configFile;

    };
}
