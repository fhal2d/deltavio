#pragma once

/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/

template <typename T, int N>
class CircularBuffer
{

public:
	CircularBuffer() { m_head = m_tail = 0; }
	
	bool empty() const
	{
		return m_head == m_tail;
	}

	bool full() const
	{
		return (m_head + 1 & M_END) == m_tail;
	}

	int getDeltaIndex(int start, int delta) const
	{
		return start + M_SIZE + delta & M_END;
	}

	T& getHeadNode()
	{
		return m_dat[m_head];
	}

	void pushIndex()
	{
		m_head = m_head + 1 & M_END;
		if (m_head == m_tail)
		{
			m_tail = m_tail + 1 & M_END;
		}
	}

	void popIndex()
	{
		m_tail = m_tail + 1 & M_END;
	}
	
	template<typename Key>
	int binarySearch(const Key&key)const 
	{
		
		//If buffer is empty,return false
		if (empty())
			return -1;
		int head_M1 = getDeltaIndex(m_head, -1);
		int tail_P1 = full()? getDeltaIndex(m_tail, 1):m_tail;

		if(m_dat[head_M1]<key||m_dat[tail_P1]>key)
			return -1;

		int left;
		int right;
		//set binary search left and right side
		if (head_M1 > tail_P1)
		{
			left = tail_P1;
			right = head_M1;
		}
		else
		{
			if (m_dat[0]<=key)
			{
				left = 0;
				right = head_M1;
			}
			else
			{
				if (m_dat[M_END]<=key)
				{
					left = M_END;
					right = 0;
				}
				else
				{
					left = tail_P1;
					right = M_END;
				}
			}
		}
		//do binary search
		while (left + 1 < right)
		{
			int mid = (left + right) / 2;
			if (m_dat[mid]==key)
				return mid;
			if (m_dat[mid]<=key)
				left = mid;
			else
				right = mid;
		}

		return left;
	}
	
protected:
	static constexpr int M_SIZE = N;
	static constexpr int M_END = M_SIZE - 1;
	
	T m_dat[M_SIZE];
	int m_head, m_tail;

};
