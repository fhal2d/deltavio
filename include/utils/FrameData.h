/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "precompile.h"
#include <memory>
using namespace DeltaVIO;

class FrameData
{
public:
    typedef std::shared_ptr<FrameData> Ptr;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	void copy(const cv::Mat& image, double timestamp)
	{
		m_image = image.clone();
		m_P.setZero();
		m_R.setIdentity();
		m_timeStamp = timestamp;
	}

    double getTimestamp(){ return m_timeStamp; }

	cv::Mat m_image;

    double m_timeStamp;

	Eigen::Vector3f m_P;				

	Eigen::Quaternionf m_R;


};
