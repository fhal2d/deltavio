/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include  <vector>
#include <string>
#include "targetDefine.h"
namespace DeltaVIO
{
	
	template <typename out>
	out stringToNumber(const std::string& valIn)
	{
		out valOut;
		std::stringstream ss;
		ss << valIn;
		ss >> valOut;
		return valOut;
	}
	/*
	 *Parse (split) a string in C++ using string delimiter (standard C++)
	 *https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
	 */
	inline std::vector<std::string> split(const std::string& str, char delimiter)
	{
		std::vector<std::string> internal;
		std::stringstream ss(str); // Turn the string into a stream.
		std::string tok;

		while (getline(ss, tok, delimiter))
		{
			internal.push_back(tok);
		}
		return internal;
	}

	/*
	 *	This function is from PTAM
	 *	https://github.com/Oxford-PTAM/PTAM-GPL
	 */	
	template<int patchSize=8>
	struct ZMSSDPatch
	{
		const int PatchArea = patchSize*patchSize;
		const int halfSize = patchSize / 2;
		uchar patch[patchSize*patchSize];
		int sum;
		int sumSq;
		ZMSSDPatch(cv::Mat&img, int u, int v)
		{
			uchar* pImgData = img.ptr<uchar>(v - halfSize + 1) + u - halfSize + 1;
			sum = 0; sumSq = 0;
			auto p = patch;
			for (int i = 0; i < patchSize; ++i)
			{
				auto r = pImgData + img.cols*i;
				for (int j = 0; j < patchSize; ++j,++p)
				{
					*p = r[j];
					sum += *p;
					sumSq += *p * *p;
				}
			}
		}

		int computeZMSSD(cv::Mat&img, int u, int v)
		{
			uchar* pImageData = img.ptr<uchar>(v - halfSize + 1) + u - halfSize + 1;
			int rightSum = 0; int rightSumSq = 0;
			auto p = patch;
			int crossSum = 0;
			for (int i = 0; i < patchSize; ++i)
			{
				auto r = pImageData + img.cols*i;
				for (int j = 0; j < patchSize; ++j, ++p)
				{
					crossSum += *p * r[j];
					rightSum += r[j];
					rightSumSq += r[j] * r[j];
				}
			}

			return ((2 * sum*rightSum - sum*sum - rightSum*rightSum) / PatchArea + rightSumSq + sumSq - 2 * crossSum);
		}
	};



}