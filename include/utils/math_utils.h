/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include  "BasicTypes.h"
#include <fstream>

namespace  DeltaVIO{

inline Matrix3f crossMat(const Vector3f& x)
{
	Matrix3f X;
	X << 0, -x(2), x(1),
		x(2), 0, -x(0),
		-x(1), x(0), 0; 
	return X;
}


inline Matrix3f vector2Jac(const Vector3f& x)
{
	return Matrix3f::Identity() - 0.5f * crossMat(x);
}

template<typename T>
T linearInterpolate(const T&a,const T&b,float k)
{
	return (1-k)*a+k*b; 
}

//This is from Android library
inline Eigen::Quaternionf getRotFromGravAndMag(const Eigen::Vector3f &gravity, const Eigen::Vector3f &magnet)
{
    float Ax = gravity[0];
    float Ay = gravity[1];
    float Az = gravity[2];
    float Ex = magnet[0];
    float Ey = magnet[1];
    float Ez = magnet[2]; 
    float Hx = Ey*Az - Ez*Ay;
    float Hy = Ez*Ax - Ex*Az;
    float Hz = Ex*Ay - Ey*Ax;
    float normH = (float)sqrt(Hx*Hx + Hy*Hy + Hz*Hz);
    if (normH < FLT_EPSILON) {
        return Eigen::Quaternionf::Identity();
    }
    float invH = 1.0f / normH;
    Hx *= invH;
    Hy *= invH;
    Hz *= invH;
    float invA = 1.0f / sqrt(Ax*Ax + Ay*Ay + Az*Az);
    Ax *= invA;
    Ay *= invA;
    Az *= invA;
    float Mx = Ay*Hz - Az*Hy;
    float My = Az*Hx - Ax*Hz;
    float Mz = Ax*Hy - Ay*Hx;
	Eigen::Matrix3f R;

	R << Hx, Hy, Hz,
		-Ax, -Ay, -Az,
		Mx, My, Mz;
	return Eigen::Quaternionf(R);
}

struct LM_Result
{
	bool bConverged;
	float cost;
	float dZMax;
	void clear()
	{
		bConverged = false;
		cost = 0;
		dZMax = 0;
	}
};

template<int nDim,typename Type>
class NonLinear_LM
{
public:
	
	NonLinear_LM(Type Epsilon1,Type Epsilon2,Type tau,int nMaxIters,bool verbose=false)
					:m_fEpsilon1(Epsilon1),m_fEpsilon2(Epsilon2),m_nMaxIters(nMaxIters),m_tau(tau),m_verbose(verbose)
	{

	};
	~NonLinear_LM()
	{
	}
	void clear();
	virtual float evaluateF(bool bNewZ) =0;
	virtual bool userDefinedDecentFail()=0;
	virtual bool userDefinedConvergeCriteria() = 0;
	void solve();
	
protected:
	Eigen::Matrix<Type,nDim,nDim> H,HTemp;
	Eigen::Matrix<Type,nDim,1> b,bTemp;
	Eigen::Matrix<Type,nDim,1> z,zNew;
	Eigen::Matrix<Type,nDim,1> dZ;
	Type m_fEpsilon1,m_fEpsilon2;
	int m_nMaxIters;
	int m_Iter;
	Type m_tau;
	bool m_verbose;
	LM_Result m_Result;

	
};

template <int nDim, typename Type>
void NonLinear_LM<nDim, Type>::clear()
{
	H.setZero();
	b.setZero();
	HTemp.setZero();
	bTemp.setZero();
	z.setZero();
	dZ.setZero();
	m_Result.clear();
}

template <int nDim, typename Type>
void NonLinear_LM<nDim, Type>::solve()
{
	
	float nu = 2;
	
	float cost0 = evaluateF(false);
	H = HTemp;
	b = bTemp;
	float mu = H.diagonal().maxCoeff() * m_tau;
	if (b.cwiseAbs().maxCoeff() < m_fEpsilon1)
		m_Result.bConverged = true;
	
	for (m_Iter=1; !m_Result.bConverged &&m_Iter <m_nMaxIters;++m_Iter)
	{
		Eigen::Matrix<Type, nDim, nDim> H_ = H + Eigen::Matrix<Type,nDim,nDim>::Identity(3, 3) * mu;
		
		dZ = H_.ldlt().solve(b);

		if (dZ.norm() < m_fEpsilon2 * (z.norm() + m_fEpsilon2)) {
			m_Result.bConverged = true;
			printf("\t%.6f / %.6f\n", dZ.norm(), m_fEpsilon2 * (z.norm() + m_fEpsilon2));
		}

		zNew = z + dZ;
		 
		if (m_verbose) {
			printf("#Iter:\t %02d\n", m_Iter);
			printf( "\t#mu:%.6f\n", mu);
			printf( "\t#dZ:");
			for (int i = 0; i < nDim; ++i)
				printf( "  %.6f", dZ[i]);
			printf( "\n\t#z:\t");
			for (int i = 0; i < nDim; ++i)
				printf( "  %.6f", z[i]);
			printf( "  ->  ");
			for (int i = 0; i < nDim; ++i)
				printf( "  %.6f", zNew[i]);
			printf( "\n");
			printf("\n\t#b:\t");
			for (int i = 0; i < nDim; ++i)
				printf("  %.6f", b[i]);
			printf("\n");
		}

		//compute rho for estimating decent performance
		float cost1 = evaluateF(true);
		float rho = (cost0 - cost1) / (0.5 * dZ.dot(mu * dZ + b));
		if (m_verbose)
			printf("\trho:%.6f\n", rho);
		if(rho>0&&!userDefinedDecentFail())
		{
			z = zNew;
			H = HTemp;
			b = bTemp;
			if (m_verbose) {
				printf( "\t#cost:\t %.6f  ->  %.6f\n", cost0, cost1);
				printf( "\t#Status: Accept\n");
			}
			cost0 = cost1;
			if (b.cwiseAbs().maxCoeff() < m_fEpsilon1)
				m_Result.bConverged = true;
			mu = mu * std::max(1.f / 3.f, 1 - std::pow(2.f * rho - 1.f, 3));
			nu = 2;

			m_Result.dZMax = dZ.cwiseAbs().maxCoeff();
			m_Result.cost = cost0;
		}
		else
		{
			mu = mu * nu;
			nu = 2 * nu;
			if (m_verbose)
				printf( "\t #Status: Reject\n");
		}

		if (m_verbose)
			printf( "--------------------------------------------------------------");
		
//		while(userDefinedDecentFail())
//		{
//			MatrixXf H2 = H;
//			H2.diagonal() *= (1 + lambda);
//			dZ = H2.llt().solve(b);
//            if (dZ.norm() < m_fEpsilon1)
//                goto converged;
//			lambda *= 10;
//            ++iter;
//		}
//
//		if (dZ.norm() < m_fEpsilon1) {
//converged:
//				m_bConverged = true;
//				break;
//			}
//
//		z+=dZ;
	}
	if (userDefinedConvergeCriteria())
		m_Result.bConverged = true;

}

	//Counting bits set, in parallel
// http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel

inline int HammingDistance(int* pa, int* pb)
{
	int dist = 0;

	for (int i = 0; i < 8; i++, pa++, pb++)
	{
		unsigned  int v = *pa ^ *pb;
		v = v - ((v >> 1) & 0x55555555);
		v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
		dist += (((v + (v >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24;

	}

	return dist;
}


	extern int rand_list[2000];
	extern float chi2LUT[80];

}
