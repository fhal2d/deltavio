/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
//std lib
#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include <thread>
#include <fstream>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <numeric>
#include <chrono>
#include <memory>
#include <targetDefine.h>



//eigen and sophus

#include "sophus/se3.hpp"

//config file
#include "Config.h"
#include "ModuleDefine.h"
#include "msckfConstexpr.h"
//opencv
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

using namespace DeltaVIO;
