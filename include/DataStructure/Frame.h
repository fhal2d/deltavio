/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <unordered_set>
#include "FilterState.h"


namespace  DeltaVIO
{
	struct TrackedFeature;
	struct Frame {
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

		Frame();

		~Frame();

		void removeLinksFromAllTrackedFeatures(TrackedFeature* ftTrack);

		void removeAllFeatures();

		CamState* state= nullptr;									//pointer to camera states including position,rotation,etc.

		std::unordered_set<TrackedFeature*> m_vTrackedFeatures;		//All tracked feature in this frame.
	};

	typedef std::shared_ptr<Frame> FramePtr;








}