/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "BasicTypes.h"

namespace DeltaVIO{
	struct ImuPreintergration
	{
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

		double t0;		//first data timestamp
		double t1;		//last data timestamp
		double dT;		//delta time

		Matrix3f dR;	//delta Rotation
		Vector3f dV;	//delta linear velocity
		Vector3f dP;	//delta position
		
		Matrix3f dRdg;  //jacobian rotation wrt gyroscope
		Matrix3f dVda;  //jacobian linear velocity wrt accelerator
		Matrix3f dVdg;	//jacobian linear velocity wrt gyroscope
		Matrix3f dPda;	//jacobian position wrt accelerator
		Matrix3f dPdg;	//jacobian position wrt gyroscope

		Matrix99f Cov;  //covariance matrix r,v,p

		void reset() {
			dT = 0;

			dR.setIdentity();
			dV.setZero();
			dP.setZero();
			Cov.setZero();

			dRdg.setZero();
			dVdg.setZero();
			dVda.setZero();
			dPdg.setZero();
			dPda.setZero();
		}
	};
}