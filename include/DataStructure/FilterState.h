/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "BasicTypes.h"


namespace DeltaVIO
{
	struct Frame;
	typedef std::shared_ptr<Frame> FramePtr;
	struct TrackedFeature;
	typedef std::shared_ptr<TrackedFeature> TrackedFeaturePtr;


	struct PointState{

		EIGEN_MAKE_ALIGNED_OPERATOR_NEW; 
		Vector3f				m_Pw;				//	point position in world frame
		Vector3f				m_Pw_FEJ;			//	point position First Estimate Jacobian

		MatrixXf			H;						//	Observation Matrix
		TrackedFeature*		host = nullptr;

		int m_id = 0;								//only used in visualizer
		PointState()
		{
			static int counter = 0;
			m_id = counter++;
		}
	};


	struct CamState{
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

		Matrix3f	m_Rwi;					// rotation matrix from imu frame to world frame
		Vector3f	m_Pwi;					// imu position in world frame
		Vector3f    m_Pw_FEJ;				// First Estimate Jacobian Imu Position in world frame

		int			m_idx;					// camera idx in sliding window		
		bool		m_bToMargin = false;	// flag to marginalize
		Frame*		m_pHost = nullptr;		// pointer to host frame

		int m_id = 0;						//only used in visualizer
		CamState()
		{
			static int counter = 0;
			m_id = counter++;
		}
	};


	


	struct MsckfState {
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

		std::vector<FramePtr>			m_vFrames;	//All frames in sliding window
		Vector3f							vel;		// linear velocity	
	};

}
