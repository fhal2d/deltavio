/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "BasicTypes.h"
#include "VisualObservation.h"
#include "FilterState.h"
#include "utils/math_utils.h"

namespace  DeltaVIO{

	struct FeaturePoint;
	struct Frame;

	struct TrackedFeature: public NonLinear_LM<3,float> {
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

		TrackedFeature();

		~TrackedFeature();

		bool	m_bDead;			//still be tracked
		int		m_nObs;				//the number of observations matched
		int		m_GridIdx;			//grid idx in this frame
		int		m_nMatchFailed;		//the number of frames mismatched
		int		m_MeanDescDist;		//the mean descriptor distance
		int		m_LastDescDist;		//the last descriptor distance
		float	m_rayAngle;			//ray angle between current camera ray and the first ray
		float	m_rayAngle0;		//last ray angle
		float	m_LastMovedPx;		//the pixel distance last frame moved

		std::vector<VisualObservation>	m_vVisualObs;		//vector of all visual observations
		Vector2f						m_PredictedPx;		//predicted pixel position using propagated camera pose
		PointState*						m_pState;			//pointer to point state
		Mat								m_desc;				//feature descriptor
		FeaturePoint*					m_pMatchedFeature;	//pointer to last matched feature

		std::vector<Matrix3f>			m_vdR;				//used in triangulate
		std::vector<Vector3f>			m_vdt;

		void addVisualObservation(FeaturePoint* featurePoint, Frame* frame);
		void popObservation();

		void removeLinksInCamStates();
		void drawFeatureTrack(cv::Mat& image, cv::Scalar color) const;

		//Triangulation
		bool userDefinedConvergeCriteria()override;
		bool triangulate();
		float evaluateF(bool bNewZ) override;
		bool userDefinedDecentFail() override;

	};

	inline TrackedFeature::~TrackedFeature()
	{
		if (!m_vVisualObs.empty())
			removeLinksInCamStates();
		if (m_pState)
		{
			delete m_pState;
			m_pState = nullptr;
		}
	}

	inline TrackedFeature::TrackedFeature():NonLinear_LM(1e-2,0.03,1e-3,6,false)
	{
		m_nMatchFailed = 0;
		m_bDead = false;
		m_nObs = 0;
		m_MeanDescDist = 90;
		m_LastDescDist = 90;
		m_rayAngle = 0;
		m_LastMovedPx = 0;
		m_pState = nullptr;
		m_pMatchedFeature = nullptr;
	}

	typedef std::shared_ptr<TrackedFeature> TrackedFeaturePtr;

}
