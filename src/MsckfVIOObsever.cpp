/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include "MsckfVIOObsever.h"
#include "FeatureTrackerDescriptor.h"
#include "ImuBuffer.h"
#include <CamModel/CamModel.h>
#include "DataStructure/VisualObservation.h"
#include "MsckfDataAssociation.h"

using namespace std;
using namespace Eigen;
using namespace cv;
namespace DeltaVIO {





	void MsckfVIOObsever::_processInputData(FrameData::Ptr framePtr)
	{
		double timestamp = framePtr->getTimestamp();
		m_pFrameNew = std::make_shared<Frame>();
		static auto& imuBuffer = ImuBuffer::getBuffer();
		printf("### Frame:%d timestamp:%lf\n", m_pFrameNew->state->m_id, timestamp);
		
		if (!m_bInited)
		{
			ImuData imuData;
			imuData.timestamp = timestamp;
			if(!imuBuffer.getDataByBinarySearch(imuData))
			{
				throw std::runtime_error("No Imu data or timestamp error");
			};

			auto q = getRotFromGravAndMag(imuBuffer.getGravity().cast<float>(), Eigen::Vector3f(0,0,1));

			framePtr->m_P.setZero();
			framePtr->m_R = q.conjugate().cast<float>();

			Matrix3f Rwi = q.toRotationMatrix();

            imuBuffer.setZeroBias();
			initialize(Rwi);

			m_imuTerm.t0 = timestamp;
			return;
		}

		
		m_imuTerm.t1 = timestamp;
		imuBuffer.imuPreIntegration(m_imuTerm);

#if !USE_OPTICAL_FLOW
		Mat halfImage;
		if (!framePtr->m_image.empty()) {
			cv::resize(framePtr->m_image, halfImage, cv::Size(framePtr->m_image.cols/2, framePtr->m_image.rows/2), 0, 0, CV_INTER_AREA);
		}

		m_pDataInputPack = std::make_shared<MsckfDataPack>(m_imuTerm, halfImage);
#else
		m_pDataInputPack = std::make_shared<MsckfDataPack>(m_imuTerm, framePtr->m_image);

#endif

		m_imuTerm.t0 = m_imuTerm.t1;
	}

	void MsckfVIOObsever::_processOutputData(FrameData::Ptr dataPtr)
	{
		Matrix3f Rwi;
		Vector3f Pwi,Vwi;
		Vector3f bg, ba;
		auto camState = m_pFrameNew->state;
		Rwi = camState->m_Rwi;
		Pwi = camState->m_Pwi;
		Vwi = m_msckfState.vel;

		dataPtr->m_P = Pwi.cast<float>()*1e3;
		dataPtr->m_R =Quaternionf(Rwi.transpose().cast<float>());

        ImuBuffer::getBuffer().getBias(bg, ba);

		Quaternionf _q(Rwi);

		static FILE * file = fopen("outputPose.csv", "w");
		static FILE * bias = fopen("bias.csv", "w");
		fprintf(file, "%lld,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
                (long long)(dataPtr->m_timeStamp*1e9), Pwi[0], Pwi[1], Pwi[2], _q.w(), _q.x(), _q.y(), _q.z(), Vwi[0], Vwi[1], Vwi[2]
		);
		fprintf(bias, "%9.6f,%9.6f,%9.6f,%9.6f,%9.6f,%9.6f\n",
			bg[0], bg[1], bg[2], ba[0], ba[1], ba[2]
		);
		printf( "%lld,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
                (long long)(dataPtr->m_timeStamp*1e9), Pwi[0], Pwi[1], Pwi[2], _q.w(), _q.x(), _q.y(), _q.z(), Vwi[0], Vwi[1], Vwi[2]
		);
#if ENABLE_VISUALIZER

		cv::Mat trackImage;
		_drawTrackImage(dataPtr,trackImage);
		m_pFrameAdapter->pushImageTexture(trackImage.data,trackImage.cols,trackImage.rows,trackImage.channels());
		m_pFrameAdapter->finishFrame();

#endif
		// imshow("FeatureTrack",trackImage);
		// waitKey(1);

	}

	void MsckfVIOObsever::_updatePointsAndCamsToVisualizer()
	{
		static std::vector<Eigen::Vector3f> vPointsGL;
		static std::vector<FrameGL> vFramesGL;
		vPointsGL.clear();
		vFramesGL.clear();
		vPointsGL.reserve(300);
		vFramesGL.reserve(100);

		for(auto lTrack:m_lTrackedFeatures)
		{
			if(lTrack->m_pState){
				vPointsGL.push_back(lTrack->m_pState->m_Pw*1000);
			}
		}

		for(auto frame:m_msckfState.m_vFrames)
		{
			vFramesGL.emplace_back(frame->state->m_Rwi,frame->state->m_Pwi*1000,frame->state->m_id);
		}

		m_pFrameAdapter->pushViewMatrix(vFramesGL);
		m_pWorldPointAdapter->pushWorldPoint(vPointsGL);
		
	}

	void MsckfVIOObsever::_drawTrackImage(FrameData::Ptr dataPtr, cv::Mat& trackImage)
	{
		cvtColor(dataPtr->m_image,trackImage,CV_GRAY2BGR);
		
		for(auto lTrack:m_lTrackedFeatures)
		{
			if(!lTrack->m_bDead){
				if (lTrack->m_nObs > 5)
					lTrack->drawFeatureTrack(trackImage, _BLUE_SCALAR);
				else
					lTrack->drawFeatureTrack(trackImage, _RED_SCALAR);
			}
		}
	}

	void MsckfVIOObsever::initialize(const Matrix3f& Rwi)
	{
		auto camState = m_pFrameNew->state;

		camState->m_Rwi = Rwi;
		camState->m_Pwi.setZero();
		camState->m_Pw_FEJ.setZero();
		camState->m_idx = 0;
		m_msckfState.m_vFrames.push_back(m_pFrameNew);

		m_solver.init(camState,&m_msckfState.vel);
		m_solver.addCamState(camState);

	}

	void MsckfVIOObsever::addIMUInformation()
	{
		m_solver.addCamState(m_pFrameNew->state);
		m_solver.propagate(&m_imuTerm);
	}

	void MsckfVIOObsever::_removeDeadFeatures()
	{
		m_lTrackedFeatures.remove_if([](const TrackedFeaturePtr& tracked_feature) { return tracked_feature->m_bDead; });
	}

	void MsckfVIOObsever::addVisionInformation()
	{
		_marginFrames();

		DataAssociation::doDataAssociation(m_lTrackedFeatures);

		_stackInformationFactorMatrix();

		m_solver.solveAndUpdateStates();
		
		_updatePointsAndCamsToVisualizer();

		_removeDeadFeatures();
	}


	void MsckfVIOObsever::_pushPoints2Grid(std::vector<TrackedFeaturePtr>& vDeadFeature)
	{
		static std::vector<std::vector<TrackedFeaturePtr> > vvGrid44(4 * 4);
        static CamModel* camModel = CamModel::getCamModel();
        static const int STEPX = camModel->width()/4;
        static const int STEPY = camModel->height()/4;

		auto comparator_less = [](const TrackedFeaturePtr&a, const TrackedFeaturePtr&b) 
		{
        	return a->m_bDead == b->m_bDead?a->m_rayAngle < b->m_rayAngle:a->m_bDead < b->m_bDead;
		};

		auto selectTop2 = [&](const std::vector<TrackedFeaturePtr>&src,std::vector<TrackedFeaturePtr>&dst)
		{
			TrackedFeaturePtr pFirst = nullptr, pSecond = nullptr;
            if (!src.empty()) {

                for (auto tracked_feature:src) {
                    if (!pSecond || comparator_less(pSecond, tracked_feature)) {
                        if (pSecond&&pSecond->m_bDead) 
							m_vTrackedFeatureNextUpdate.push_back(pSecond);  
                        pSecond = tracked_feature;
                        if (!pFirst || comparator_less(pFirst, pSecond)) {
                            std::swap(pFirst, pSecond);
                        }
                    }
                    else {
                        if (tracked_feature->m_bDead)
                            m_vTrackedFeatureNextUpdate.push_back(tracked_feature);
                    }
                }
                if (pSecond) 
                    dst.push_back(pSecond);
                if (pFirst&&pFirst != pSecond) 
                    dst.push_back(pFirst);
            }
		};
		for (auto deadFeature : vDeadFeature) {
			auto&ob = deadFeature->m_vVisualObs.back();
            vvGrid44[int(ob.m_px.x()/STEPX) + 4 * int(ob.m_px.y()/STEPY)].push_back(deadFeature);
        }


		static int LUT[16] = 
		{ 0,0,1,1,
			0,0,1,1,
			2,2,3,3,
			2,2,3,3 };

        for (int i = 0; i < 16; ++i) {
			selectTop2(vvGrid44[i], m_Grid22[LUT[i]]);
        }

        for (auto&grid : m_Grid22) {
            std::sort(grid.begin(), grid.end(), comparator_less);
        }

		for (auto&grid : vvGrid44)
			grid.clear();
	}

	void MsckfVIOObsever::_addBufferPoints(std::vector<TrackedFeaturePtr>& vDeadFeature)
	{		
		constexpr int MAX_BUFFER_OBS = 5;
		for (auto tracked_feature : m_vTrackedFeatureNextUpdate)
        {
            if (tracked_feature->m_vVisualObs.size() > MAX_BUFFER_OBS)
                vDeadFeature.push_back(tracked_feature);
            else
                tracked_feature->removeLinksInCamStates();
        }
        m_vTrackedFeatureNextUpdate.clear();
	}

	void MsckfVIOObsever::_addDeadPoints(
		std::vector<TrackedFeaturePtr>& vDeadFeature)
	{
		constexpr int MAX_OBS = 10;
		for (auto iter = m_lTrackedFeatures.begin(); iter != m_lTrackedFeatures.end();) {
            auto tracked_feature = *iter;

            if (tracked_feature->m_bDead) {
                if (tracked_feature->m_nObs >= MAX_OBS)
                    vDeadFeature.push_back(tracked_feature);
                else 
                    tracked_feature->removeLinksInCamStates();
                iter = m_lTrackedFeatures.erase(iter);
                continue;
            }
            if (tracked_feature->m_vVisualObs.size() >= MAX_OBS) 
                vDeadFeature.push_back(tracked_feature);
            ++iter;
        }
	}

	void MsckfVIOObsever::_selectFrames2Margin()
	{
			int nCams = m_msckfState.m_vFrames.size();
			int cnt = 0;
			for (auto frame:m_msckfState.m_vFrames)
			{
				if (frame->m_vTrackedFeatures.empty())
				{
					cnt++;
					frame->removeAllFeatures();
					frame->state->m_bToMargin = true;
				}
			}
			if (!cnt&&nCams >= MAX_CAMERA_STATE)
			{
				static int camIdxToMargin = 0;
				camIdxToMargin += CAM_DELETE_STEP;
				if (camIdxToMargin >= nCams - 1)
					camIdxToMargin = 1;

				m_msckfState.m_vFrames[camIdxToMargin]->removeAllFeatures();
				m_msckfState.m_vFrames[camIdxToMargin]->state->m_bToMargin = true;
			}
	}

	void MsckfVIOObsever::_marginFrames()
	{
		std::vector<FramePtr> vCamStatesNew;

		_selectFrames2Margin();
		
		m_solver.marginalize();

		for(auto frame:m_msckfState.m_vFrames)
			if(!frame->state->m_bToMargin)
				vCamStatesNew.push_back(frame);
		m_msckfState.m_vFrames = vCamStatesNew;
		m_msckfState.m_vFrames.push_back(m_pFrameNew);
		
	}

	void MsckfVIOObsever::_selectPointsToUpdate()
	{
	    m_vTrackedFeatureToUpdate.clear();
		
        static std::vector<TrackedFeaturePtr> vDeadFeature;

		_addBufferPoints(vDeadFeature);

		_addDeadPoints(vDeadFeature);

		_pushPoints2Grid(vDeadFeature);

		vDeadFeature.clear();
	}

	void MsckfVIOObsever::_tryAddMsckfPoseConstraint()
	{
		int nPointsPerGrid = MAX_MSCKF_FEATURE / 4;
		int nPointsLeft = MAX_MSCKF_FEATURE;

		auto triangleAndVerify = [&](TrackedFeaturePtr& track)
		{
			if (track->triangulate())
			{
				m_solver.computeMsckfConstraint(track);

				return m_solver.MahalanobisTest(track->m_pState);

			}
			return false;
		};
		auto selectPoints = [&]()
		{
			for (auto& grid : m_Grid22)
			{
				int nPointsAdded = 0;
				while (nPointsAdded < nPointsPerGrid && !grid.empty())
				{
					auto& ft = grid.back();
					if (triangleAndVerify(ft))
					{
						++nPointsAdded;
						--nPointsLeft;


						m_solver.addVisualObservation(ft->m_pState);

						m_vTrackedFeatureToUpdate.push_back(ft);
						ft->m_bDead = true;
					}
					grid.pop_back();
				}
			}
		};
		auto bufferPoints = [&]()
		{
			for (auto& grid : m_Grid22)
			{
				for (auto& ft : grid)
				{
					if (ft->m_bDead)
					{
						m_vTrackedFeatureNextUpdate.push_back(ft);
					}
				}
				grid.clear();
			}
		};

		selectPoints();
		nPointsPerGrid = nPointsLeft / 4;

		if (nPointsPerGrid)
			selectPoints();
		bufferPoints();
	}

	void MsckfVIOObsever::_stackInformationFactorMatrix()
	{
		int nDIM = m_solver.stackInformationFactorMatrix();
		if(!nDIM)
		{
			if (_DetectStill())
			{
				m_solver.addVelocityConstraint();
			}
		}

	}

	bool MsckfVIOObsever::_DetectStill()
	{
		float max_parallax = 0;
		int n = 0;
		for (auto ftTrack : m_lTrackedFeatures)
		{
			if (ftTrack->m_vVisualObs.size() >= 2)
			{
				max_parallax += sqrt(ftTrack->m_LastMovedPx);
				++n;
			}
		}
		float lastPx = n ? max_parallax / n : 5;
		static int nFrames = 0;
		static int nStaticFrames = 0;
		float pxThres = 0.5;
		nFrames++;
		if (nFrames < 10)
			pxThres = 2;


		if (lastPx < pxThres)
		{
			nStaticFrames++;
			if (nStaticFrames >= 5)
			{
				return true;
			}
		}
		else
		{
			nStaticFrames = 0;
		}
		return false;
	}

	void MsckfVIOObsever::_testVisionModule()
	{
		
		m_msckfState.m_vFrames.push_back(m_pFrameNew);
		if(!m_pTrackerOpticalFlow)
			m_pTrackerOpticalFlow = new FeatureTrackerOpticalFlow(150);

		m_pTrackerOpticalFlow->matchNewFrame(m_lTrackedFeatures, m_pDataInputPack->image, m_pFrameNew.get());

		m_lTrackedFeatures.remove_if([](auto& lf) {return lf->m_bDead; });

		
	}

	int MsckfVIOObsever::trackFrame(FrameData::Ptr imageDataPtr)
	{
		_processInputData(imageDataPtr);

		if(!m_bInited){
			m_bInited = true;
			return 0;
		}

		//propagate step in EKF
		addIMUInformation();

#if TEST_OPTICAL_FLOW

		_testVisionModule();
#else
		
#if USE_OPTICAL_FLOW
		m_pTrackerOpticalFlow->matchNewFrame(m_lTrackedFeatures, m_pDataInputPack->image, m_pFrameNew.get());
#else
		//match feature point
		m_pVisionModule->matchNewFrame(m_lTrackedFeatures,m_pDataInputPack->image, m_pFrameNew.get());
#endif
		//update step in EKF
		addVisionInformation();

		_processOutputData(imageDataPtr);
#endif
        return 0;
	}

	MsckfVIOObsever::MsckfVIOObsever()
	{
		DataAssociation::initDataAssociation(&m_solver);
		
		m_pVisionModule = new FeatureTrackerDescriptor();
#if USE_OPTICAL_FLOW
		m_pTrackerOpticalFlow = new FeatureTrackerOpticalFlow(350);
#endif
	}

	MsckfVIOObsever::~MsckfVIOObsever()
	{
		if(m_pVisionModule)
			delete m_pVisionModule;
		if (m_pTrackerOpticalFlow)
			delete m_pTrackerOpticalFlow;
	}

}

