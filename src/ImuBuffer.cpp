/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include <sophus/se3.hpp>
#include "ImuBuffer.h"
#include "DataStructure/ImuPreintergration.h"
#include "utils/math_utils.h"

namespace DeltaVIO
{
	ImuBuffer::ImuBuffer(): CircularBuffer<ImuData, 512>()
	{
		m_GyroBias.setZero();
		m_AccBias.setZero();

		m_NoiseCov.setIdentity(6,6);
		m_NoiseCov.topLeftCorner(3,3) *= Configuration::GyroNoise2;
		m_NoiseCov.bottomRightCorner(3,3) *= Configuration::AccNoise2;
	}


	int ImuBuffer::findDataByBinarySearch(double timestamp) const
	{
		return binarySearch<double>(timestamp);
	}

	void ImuBuffer::updateBias(const Vector3f& dBg, const Vector3f& dBa)
	{
		m_GyroBias += dBg;
		m_AccBias += dBa;
	}

	void ImuBuffer::setBias(const Vector3f& bg, const Vector3f& ba)
	{
		m_GyroBias = bg;
		m_AccBias = ba;
	}

	void ImuBuffer::setZeroBias()
	{
		m_GyroBias.setZero();
		m_AccBias.setZero();
	}

	void ImuBuffer::getBias(Vector3f& bg, Vector3f& ba) const
	{
		bg = m_GyroBias;
		ba = m_AccBias;
	}

	Vector3f ImuBuffer::getGravity()
	{
		std::lock_guard<std::mutex> lck(m_gMutex);
		return mGravity;
	}

	bool ImuBuffer::getDataByBinarySearch(ImuData& imuData) const
	{
		int index = findDataByBinarySearch(imuData.timestamp);
		if (index < 0)
		{
			throw std::runtime_error("No Imu data found,Please check timestamp");
		}

		auto& left = m_dat[index];
		auto& right = m_dat[getDeltaIndex(index, 1)];


		//linear interpolation
		float k = (imuData.timestamp - left.timestamp) * 1.0 / (right.timestamp - left.timestamp);
		imuData.gyro = linearInterpolate(left.gyro,right.gyro,k);
		imuData.acc = linearInterpolate(left.acc,right.acc,k);

		return index>=0;
	}


	/*----------------------------------------------------------------------------
	* IMU Preintegration on Manifold for Efficient Visual-Inertial Maximum-a-Posteriori Estimation"
	* http://www.roboticsproceedings.org/rss11/p06.pdf
	*/
	bool ImuBuffer::imuPreIntegration(ImuPreintergration& ImuTerm) const
	{
		if (ImuTerm.t0 >= ImuTerm.t1)
		{
			return false;
		}
		int Index0 = findDataByBinarySearch(ImuTerm.t0);
		int Index1 = findDataByBinarySearch(ImuTerm.t1);
		if (Index0 < 0 || Index1 < 0)
		{
			std::runtime_error("No Imu data found.Please check timestamp");
		}
		ImuTerm.reset();

		Matrix3f& dRdg = ImuTerm.dRdg;
		Matrix3f& dPda = ImuTerm.dPda;
		Matrix3f& dPdg = ImuTerm.dPdg;
		Matrix3f& dVda = ImuTerm.dVda;
		Matrix3f& dVdg = ImuTerm.dVdg;

		Matrix3f& dR = ImuTerm.dR;
		Vector3f& dV = ImuTerm.dV;
		Vector3f& dP = ImuTerm.dP;
		Matrix3f dR0 = dR;
		Vector3f dV0 = dV;

		Matrix99f& cov = ImuTerm.Cov;
		
		MatrixXf A = MatrixXf::Identity(9, 9);
		MatrixXf B = MatrixXf::Zero(9, 6);


		int indexEnd = getDeltaIndex(Index1, 1);

		int index = Index0;
		
		float dt;

		Vector3f gyro, acc;

		while (index != indexEnd)
		{
			auto& imuData = m_dat[index];
			int nextIndex = getDeltaIndex(index, 1);
			auto& nextImuData = m_dat[nextIndex];

			if (index == Index0)
			{
				dt = nextImuData.timestamp - ImuTerm.t0;
				float k = (nextImuData.timestamp - (ImuTerm.t0 + nextImuData.timestamp) / 2) / (nextImuData.timestamp - imuData.timestamp);
				gyro = linearInterpolate(imuData.gyro,nextImuData.gyro,1-k);
				acc = linearInterpolate(imuData.acc,nextImuData.acc,1-k);
			}
			else if (index == Index1)
			{
				dt = ImuTerm.t1 - imuData.timestamp;
				float k = (nextImuData.timestamp - (ImuTerm.t1 + imuData.timestamp) / 2) / (nextImuData.timestamp - imuData.timestamp);
				gyro = linearInterpolate(imuData.gyro,nextImuData.gyro,1-k);
				acc = linearInterpolate(imuData.acc,nextImuData.acc,1-k);
			}
			else
			{
				dt = nextImuData.timestamp - imuData.timestamp;
				gyro = (imuData.gyro + nextImuData.gyro) * 0.5f;
				acc = (imuData.acc + nextImuData.acc) * 0.5f;
			}

			gyro -= m_GyroBias;
			acc -= m_AccBias;

			Vector3f ddV0 = acc*dt;
			Vector3f ddR0 = gyro*dt;

			Matrix3f ddR = Sophus::SO3Group<float>::exp(ddR0).matrix();

			//update covariance iteratively
			A.topLeftCorner(3,3) = ddR.transpose();
			A.block<3, 3>(3, 0) = -dR0 * crossMat(ddV0); 
			A.bottomLeftCorner(3,3) = 0.5*dt *A.block<3, 3>(3, 0);
			A.block<3, 3>(6, 3) = Matrix3f::Identity() * dt;

			B.topLeftCorner(3,3) = vector2Jac(ddR0) * dt;
			B.block<3, 3>(3, 3) = dR0 * dt;
			B.block<3, 3>(6, 3) = dR0 * (0.5 * dt * dt);

			cov = A * cov * A.transpose() + B * m_NoiseCov * B.transpose();

			//update Jacobian Matrix iteratively
			
			dPdg += dVdg * dt + A.bottomLeftCorner(3,3) * dRdg;
			dPda += dVda * dt - dR0 * (0.5 * dt * dt);
			dVdg -= dR0 * crossMat(ddV0) * dRdg;
			dVda -= dR0 * dt;
			dRdg = ddR.transpose() * dRdg - vector2Jac(ddR0) * dt;

			//update delta states iteratively
			Vector3f ddV = dR0 * acc * dt;
			dP += dV0 * dt + 0.5* ddV * dt;
			dR0 = dR = dR0 * ddR;
			dV0 = dV = dV0 + ddV;
			

			index = nextIndex;
		}

		// add time
		ImuTerm.dT += ImuTerm.t1 - ImuTerm.t0;

		return true;
	}
	

	void ImuBuffer::ImuReceived(float* data, double timestamp)
	{
		auto& imuData = getHeadNode();
		imuData.timestamp = timestamp;
		imuData.gyro << data[0], data[1], data[2];
		imuData.acc << data[3], data[4], data[5];

		//do low pass filter to get gravity
		static Eigen::Vector3f gravity = imuData.acc;
		gravity = 0.95f * gravity + 0.05f * imuData.acc;
		{
			std::lock_guard<std::mutex> lck(m_gMutex);
			mGravity = gravity;
		}
		pushIndex();
	}


}
