/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include "ImageBuffer.h"

namespace DeltaVIO {

	ImageBuffer::ImageBuffer(): CircularBuffer<FrameData::Ptr, 4>()
	{
		for (int i = 0; i < M_SIZE; i++) {
			m_dat[i] = std::make_shared<FrameData>();
		}
	}

	bool ImageBuffer::pushImage(double timestamp, const cv::Mat& image)
	{

		m_dat[m_head]->copy(image, timestamp);

		pushIndex();
		
		return true;
	}

	FrameData::Ptr ImageBuffer::popImage()
	{
		if (empty()) return nullptr;

		int tail = m_tail;

		popIndex();

		return m_dat[tail];
	}

}
