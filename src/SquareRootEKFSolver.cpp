/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"

#include "SquareRootEKFSolver.h"
#include "DataStructure/FilterState.h"
#include "DataStructure/ImuPreintergration.h"
#include <Eigen/Dense>
#include "DataStructure/TrackedFeature.h"
#include "ImuBuffer.h"
#include "CamModel/CamModel.h"

namespace DeltaVIO
{
	SquareRootEKFSolver::SquareRootEKFSolver()
	{
	}

	void SquareRootEKFSolver::init(CamState* pCamState, Vector3f* vel)
	{
		VectorXf p(NEW_STATE_DIM);
		
		//Init information Matrix
		switch (Configuration::DatasetType)
		{
		case Configuration::EUROC:
			p << 1e-4, 1e-4, 1e-4,
				 1e-2, 1e-2, 1e-2,
				 1e-4, 1e-4, 1e-4,
				 1e-2, 1e-2, 1e-2,
				 1e-3, 1e-3, 1e-3;
			break;
		}
		m_pVel = vel;
		m_infoFactorMatrix = p.cwiseSqrt().cwiseInverse().asDiagonal();
		m_pVel->setZero();
		m_vCamStates.push_back(pCamState);
	}

	void SquareRootEKFSolver::addCamState(CamState* state)
	{
		m_pLastState = m_pNewState;
		m_pNewState = state;
	}

	void SquareRootEKFSolver::propagate(const ImuPreintergration* pImuTerm)
	{

		static Vector3f gravity(0, GRAVITY, 0);
		Matrix3f R0 = m_pLastState->m_Rwi;

		float dt = pImuTerm->dT;
		// propagate states
		m_pNewState->m_Pwi = m_pLastState->m_Pwi + (R0 * pImuTerm->dP + *m_pVel * dt + gravity * (0.5f * dt * dt));
		m_pNewState->m_Pw_FEJ = m_pNewState->m_Pwi;
		*m_pVel += R0 * pImuTerm->dV + gravity * dt;
		m_pNewState->m_Rwi = R0 * pImuTerm->dR;

		//Make state transition matrix F
		Eigen::Matrix<float,NEW_STATE_DIM,NEW_STATE_DIM> StateTransitionMatrix;
		StateTransitionMatrix.setIdentity();
		StateTransitionMatrix.block<3, 3>(0, 0) = pImuTerm->dR.transpose();
		StateTransitionMatrix.block<3, 3>(0, 6) = pImuTerm->dRdg;
		StateTransitionMatrix.block<3, 3>(3, 0) = -R0 * crossMat(pImuTerm->dP);
		StateTransitionMatrix.block<3, 3>(3, 6) = R0 * pImuTerm->dPdg;
		StateTransitionMatrix.block<3, 3>(3, 9) = Matrix3f::Identity() * dt;
		StateTransitionMatrix.block<3, 3>(3, 12) = R0 * pImuTerm->dPda;
		StateTransitionMatrix.block<3, 3>(9, 0) = -R0 * crossMat(pImuTerm->dV);
		StateTransitionMatrix.block<3, 3>(9, 6) = R0 * pImuTerm->dVdg;
		StateTransitionMatrix.block<3, 3>(9, 12) = R0 * pImuTerm->dVda;

		//make noise transition matrix
		Eigen::Matrix<float,NEW_STATE_DIM,9> NoiseTransitionMatrix;
		NoiseTransitionMatrix.setZero();
		NoiseTransitionMatrix.block<3, 3>(0, 0).setIdentity(); 
		NoiseTransitionMatrix.block<3, 3>(3, 6) = R0;
		NoiseTransitionMatrix.block<3, 3>(9, 3) = R0; 

		//Make Noise Covariance Matrix Q
		Eigen::Matrix<float,NEW_STATE_DIM,NEW_STATE_DIM> NoiseCov = NoiseTransitionMatrix * pImuTerm->Cov * NoiseTransitionMatrix.transpose();
		NoiseCov.block<3, 3>(6, 6) = Matrix3f::Identity() * (Configuration::GyroBiasNoise2 * dt * dt);
		NoiseCov.block<3, 3>(12, 12) = Matrix3f::Identity() * (Configuration::AccBiasNoise2 * dt * dt);

		Eigen::Matrix<float,NEW_STATE_DIM,NEW_STATE_DIM> NoiseFactor;
		NoiseFactor.setIdentity();
		Eigen::LLT<MatrixXf> chol(NoiseCov);
		chol.matrixU().solveInPlace(NoiseFactor);

		//Make information factor matrix
		int N = m_infoFactorMatrix.rows();
		m_infoFactorMatrix.conservativeResize(N + NEW_STATE_DIM, N + NEW_STATE_DIM);

		m_infoFactorMatrix.topRightCorner(N, NEW_STATE_DIM).setZero();
		m_infoFactorMatrix.bottomLeftCorner(NEW_STATE_DIM, N).setZero();
		m_infoFactorMatrix.block<NEW_STATE_DIM, NEW_STATE_DIM>(N, N - NEW_STATE_DIM) = NoiseFactor * StateTransitionMatrix;
		m_infoFactorMatrix.bottomRightCorner<NEW_STATE_DIM, NEW_STATE_DIM>() = -NoiseFactor;

		// make residual vector
		m_residual = VectorXf::Zero(m_infoFactorMatrix.rows());

	}

	void SquareRootEKFSolver::marginalize()
	{
		int iDim = 0;
		std::vector<int> v_MarginDIM,v_RemainDIM;
		std::vector<CamState*> v_CamStateNew;
		for (int i=0,n = m_vCamStates.size();i<n;++i)
		{
			auto state = m_vCamStates[i];
			if (state->m_bToMargin)
				for (int i = 0; i < CAM_DIM; i++)
					v_MarginDIM.push_back(iDim++);
			else
			{
				for (int i = 0; i < CAM_DIM; i++)
					v_RemainDIM.push_back(iDim++);
				v_CamStateNew.push_back(state);
			}
		}
		m_vCamStates = v_CamStateNew;
		m_vCamStates.push_back(m_pNewState);
		for (int i=0,n=m_vCamStates.size();i<n;++i)
		{
			m_vCamStates[i]->m_idx = i;
		}

		for (int i = 0; i < IMU_DIM; i++)
		{
			v_MarginDIM.push_back(iDim++);
		}
		for (int i = 0; i < NEW_STATE_DIM; i++)
		{
			v_RemainDIM.push_back(iDim++);
		}

		int nCols = m_infoFactorMatrix.cols();

		//rearrange new information factor matrix
		MatrixXf NewInfoFactor(nCols, nCols);
		int index = 0;
		for (auto idx : v_MarginDIM)
		{
			NewInfoFactor.col(index++) = m_infoFactorMatrix.col(idx);
		}
		for (auto idx : v_RemainDIM)
		{
			NewInfoFactor.col(index++) = m_infoFactorMatrix.col(idx);
		}

		//use qr to marginalize states
		Eigen::HouseholderQR<MatrixXf> qr(NewInfoFactor);
		int nRemain = v_RemainDIM.size();

		m_infoFactorMatrix = qr.matrixQR().bottomRightCorner(nRemain, nRemain).triangularView<Eigen::Upper>();

		m_infoFactorInverseMatrix = MatrixXf::Identity(nRemain, nRemain);
		m_infoFactorMatrix.triangularView<Eigen::Upper>().solveInPlace(m_infoFactorInverseMatrix);

		m_residual.resize(m_infoFactorMatrix.rows(), 1);
		m_residual.setZero();



	}
	


	bool SquareRootEKFSolver::MahalanobisTest(PointState* state)
	{		
		VectorXf z = state->H.rightCols<1>();
		int nExceptPoint = state->H.cols() - 4;
		int nObs = state->H.rows();
		MatrixXf S = MatrixXf::Zero(nObs, nObs);
		MatrixXf R = MatrixXf::Identity(nObs, nObs) * Configuration::ImageNoise2 * 2;


		MatrixXf B = state->H.middleCols(3, nExceptPoint) * m_infoFactorInverseMatrix.topLeftCorner(
																			   nExceptPoint,
						                                                           nExceptPoint).triangularView<Eigen::Upper>();
		S.noalias() += B * B.transpose() + R;


		float phi = z.transpose() * S.llt().solve(z);

		assert(nObs<80);
		return phi < chi2LUT[nObs];
	}

	void SquareRootEKFSolver::computeMsckfConstraint(TrackedFeaturePtr track)
	{
		// observation number
		int index = 0;

		static CamModel* camModel = CamModel::getCamModel();
		static Vector3f Pic = camModel->getPic();
		static Matrix3f Rci = camModel->getRci();

		int nCams = m_vCamStates.size();

		const int camStateIdx = 3;
		const int residualIdx = 3 + CAM_DIM * nCams + IMU_DIM;

		int nObs = track->m_vVisualObs.size();

		MatrixXf H;
		H.setZero(nObs * 2, 3 + (CAM_DIM * nCams + IMU_DIM) + 1);
		auto calcObsJac = [&](VisualObservation* ob)
		{
			int cam_id = ob->m_linkFrame->state->m_idx;

			Matrix3f Riw = ob->m_linkFrame->state->m_Rwi.transpose();
			Vector3f Pc = Riw * (track->m_pState->m_Pw - ob->m_linkFrame->state->m_Pwi);
			Vector3f Pc_FEJ = Riw * (track->m_pState->m_Pw_FEJ - ob->m_linkFrame->state->m_Pw_FEJ);

			H.block<2, 1>(2 * index, residualIdx) = ob->m_px - camModel->imuToImage(Pc);

			Matrix23f J23;
			camModel->camToImage(Rci * (Pc_FEJ - Pic), J23);

			H.block<2, 3>(2 * index, camStateIdx + CAM_DIM * cam_id) = J23 * (Rci * crossMat(Pc_FEJ));
			H.block<2, 3>(2 * index, camStateIdx + CAM_DIM * cam_id + 3) = -J23 * (Rci * Riw);
			H.block<2, 3>(2 * index, 0) = J23 * (Rci * Riw);
		};


		for (auto& ob : track->m_vVisualObs)
		{
			calcObsJac(&ob);
			index++;
		}

		//do null space trick to get pose constraint
		Eigen::HouseholderQR<MatrixXf> qr(H);
		track->m_pState->H = qr.matrixQR().bottomRightCorner(2 * nObs - 3, H.cols() - 3);
	}

	void SquareRootEKFSolver::addVisualObservation(PointState* state)
	{
		m_vPointStates.push_back(state);
	}

	void SquareRootEKFSolver::addVelocityConstraint()
	{
		static float invSigma = 1.0 / 1e-2;
		int nRows = m_StackedMatrix.rows();
		int nCols = m_StackedMatrix.cols();
		m_StackedMatrix.conservativeResize(nRows + 3, Eigen::NoChange);
		m_StackedMatrix.bottomRows<3>().setZero();
		m_StackedMatrix.block<3, 3>(nRows, nCols - 7) = Matrix3f::Identity() * invSigma;

		m_StackedMatrix.bottomRightCorner<3, 1>() = -*m_pVel * invSigma;

	}

	int SquareRootEKFSolver::stackInformationFactorMatrix()
	{
		int nOldStates = m_infoFactorMatrix.rows();

		int nTotalObs = 0;

		for (auto& track : m_vPointStates)
		{
			nTotalObs += track->host->m_vVisualObs.size() * 2 - 3;
		}

		m_StackedMatrix.resize(nOldStates + nTotalObs, nOldStates + 1);
		m_StackedMatrix.topLeftCorner(nOldStates, nOldStates) = m_infoFactorMatrix;
		m_StackedMatrix.topRightCorner(nOldStates, 1) = m_residual;

		float invSigma = 1 / sqrt(Configuration::ImageNoise2);
		MatrixXf H_j;
		int rffIdx = nOldStates;

		for (auto& track : m_vPointStates)
		{
			int nObs = track->H.rows();
			track->H *= invSigma;
			
			m_StackedMatrix.block(rffIdx, 0, nObs, nOldStates) =
				track->H.leftCols(nOldStates).triangularView<Eigen::Upper>();
			
			m_StackedMatrix.block(rffIdx, nOldStates, nObs, 1) = track->H.rightCols<1>();

			rffIdx += nObs;
		}

		return nTotalObs;
	}

	void SquareRootEKFSolver::solveAndUpdateStates()
	{
		bool haveNewInformation = m_StackedMatrix.rows() != m_infoFactorMatrix.rows();
		if(haveNewInformation)
		{
			int nRows = m_infoFactorMatrix.rows();
			Eigen::HouseholderQR<MatrixXf> qr(m_StackedMatrix);
			m_infoFactorMatrix =
			qr.matrixQR().topLeftCorner(nRows, nRows).triangularView<Eigen::Upper>();

			m_residual = qr.matrixQR().topRightCorner(nRows, 1);
		}

		VectorXf dx = m_infoFactorMatrix.triangularView<Eigen::Upper>().solve(m_residual);

		
		int i = 0;
		for (auto& camState : m_vCamStates)
		{
			camState->m_Rwi = camState->m_Rwi * Sophus::SO3Group<float>::exp(dx.segment<3>(i)).matrix();
			i += 3;
			camState->m_Pwi += dx.segment<3>(i);
			i += 3;
		}
		auto&imuBuffer = ImuBuffer::getBuffer();
		imuBuffer.updateBias(dx.segment<3>(i),dx.segment<3>(i+6));
		*m_pVel += dx.segment<3>(i+3);
		m_vPointStates.clear();
	}
}
