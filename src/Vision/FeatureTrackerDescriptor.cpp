/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include "FeatureTrackerDescriptor.h"
#include "fast/fast.h"
#include <CamModel/CamModel.h>
#include "utils/utils.h"
#include "DataStructure/Frame.h"
#include "DataStructure/VisualObservation.h"
#include "msckfConstexpr.h"
#include "MsckfDataAssociation.h"


using namespace cv;
using namespace std;
using namespace Eigen;

namespace DeltaVIO
{
	FeatureTrackerDescriptor::FeatureTrackerDescriptor():FeatureTracker()
	{
		_setGridAndNeighbors();
		m_pORBExtractor = new ORBextractor(500, 1.23, 4, 15, 20, 240, 320);
		m_pTwoPointRansac = new TwoPointRansac();
		m_nCorners = 0;
	}

	FeatureTrackerDescriptor::~FeatureTrackerDescriptor()
	{
	}


	void FeatureTrackerDescriptor::_setGridAndNeighbors()
	{
		int width = CamModel::getCamModel()->width();
		int height = CamModel::getCamModel()->height();
		m_nStepX = 20;
		m_nStepY = 20;
		m_nXGrid = width % m_nStepX ? width / m_nStepX + 1 : width / m_nStepX;
		m_nYGrid = height % m_nStepY ? height / m_nStepY + 1 : height / m_nStepY;
		int nGridNums = m_nXGrid*m_nYGrid;
		m_vvGridPoints.resize(nGridNums);
		m_vvGridTrackedFeature.resize(nGridNums);
		m_vvGridAllFeatures.resize(nGridNums);
		m_vvNeighbor_LUT.resize(nGridNums);
		
		for (int i = 0; i < m_nXGrid; i++)
		{
			for (size_t j = 0; j < m_nYGrid; j++)
			{
				int idx = j * m_nXGrid + i;
				auto& neighbor = m_vvNeighbor_LUT[idx];
				if (i != 0)
					neighbor.push_back(idx - 1);
				if (i != m_nXGrid - 1)
					neighbor.push_back(idx + 1);
				if (j != 0)
					neighbor.push_back(idx - m_nXGrid);
				if (j != m_nYGrid - 1)
					neighbor.push_back(idx + m_nXGrid);
				if (j != 0 && i != 0)
					neighbor.push_back(idx - m_nXGrid - 1);
				if (j != 0 && i != m_nXGrid - 1)
					neighbor.push_back(idx - m_nXGrid + 1);
				if (i != 0 && j != m_nYGrid - 1)
					neighbor.push_back(idx + m_nXGrid - 1);
				if (i != m_nXGrid - 1 && j != m_nYGrid - 1)
					neighbor.push_back(idx + m_nXGrid + 1);
				neighbor.push_back(idx);
			}
		}
		for (auto& v : m_vvGridPoints)
			v.reserve(200);

		for (auto& v : m_vvGridAllFeatures)
			v.reserve(200);

	}

	void FeatureTrackerDescriptor::_detectFeatures(bool bInit, int detection_threshold)
	{
		std::for_each(m_vvGridPoints.begin(), m_vvGridPoints.end(), [](std::vector<FeaturePoint*>& v) { v.clear(); });

		std::vector<KeyPoint> vPts;
		Mat desc;
		m_pORBExtractor->operator()(img1, Mat(), vPts, desc);

		auto pCorner = m_pFeaturePool + m_nCorners;
		int nCorner = vPts.size();
		m_nCorners += nCorner;

		for (int i = 0; i < nCorner; ++i)
		{
			auto feature_point = pCorner + i;
			feature_point->m_bIsORB = true;
			feature_point->m_pTrackedFeature = nullptr;
			feature_point->m_corner = std::move(vPts[i]);
			feature_point->m_desc = desc.row(i);

			feature_point->px.x() = feature_point->m_corner.pt.x * 2 + 0.5;
			feature_point->px.y() = feature_point->m_corner.pt.y * 2 + 0.5;
			feature_point->m_GridIdx = int(feature_point->px.x() / m_nStepX) + int(feature_point->px.y() / m_nStepY) * m_nXGrid;
			if (!bInit)
				m_vvGridPoints[feature_point->m_GridIdx].push_back(feature_point);
		}

		if (!bInit)
		{
			std::vector<fast::fast_xy> fast_corners;
			_detect_fast(img1, fast_corners, detection_threshold);
			pCorner = m_pFeaturePool + m_nCorners;
			nCorner = fast_corners.size();
			m_nCorners += nCorner;
			for (int i = 0; i < nCorner; ++i)
			{
				auto feature_point = pCorner + i;
				feature_point->m_corner.pt.x = fast_corners[i].x;
				feature_point->m_corner.pt.y = fast_corners[i].y;

				feature_point->px.x() = fast_corners[i].x * 2 + 0.5;
				feature_point->px.y() = fast_corners[i].y * 2 + 0.5;

				feature_point->m_GridIdx = int(feature_point->px.x() / m_nStepX) + int(feature_point->px.y() / m_nStepY) * m_nXGrid;
				feature_point->m_bIsORB = false;
				feature_point->m_pTrackedFeature = nullptr;

				m_vvGridPoints[feature_point->m_GridIdx].push_back(feature_point);
			}
		}
	}

	void FeatureTrackerDescriptor::_setCommonData(std::list<TrackedFeaturePtr>& trackFeatures,
	                                  Mat& image, Frame* camState)
	{
		m_pvTrackedFeatures = &trackFeatures;
		img1 = image;
		frame1 = camState;
		m_nCorners = 0;
	}

	void FeatureTrackerDescriptor::_switchFrame()
	{
		frame0 = frame1;
		img0 = img1;
	}

	void FeatureTrackerDescriptor::matchNewFrame(std::list<TrackedFeaturePtr>& trackFeatures,
	                              Mat& image, Frame* camState)
	{
		_setCommonData(trackFeatures, image, camState);
		if (trackFeatures.empty())
			_extractFeatures();
		else
			_track();

		_switchFrame();
	}

	void FeatureTrackerDescriptor::_extractFeatures()
	{

		_detectFeatures(true, 15);

		for (int i = 0; i < m_nCorners; ++i)
		{
			auto pCorner = m_pFeaturePool + i;
			if (!pCorner->m_bIsORB) continue;
			TrackedFeaturePtr tracked_feature = std::make_shared<TrackedFeature>();
			tracked_feature->addVisualObservation(pCorner, frame1);
			tracked_feature->m_GridIdx = pCorner->m_GridIdx;
			tracked_feature->m_desc = pCorner->m_desc;
			m_pvTrackedFeatures->push_back(tracked_feature);
		}
	}

	void FeatureTrackerDescriptor::_track()
	{
		_detectFeatures(false, 15);
		_trackNewFrame();
	}


	void FeatureTrackerDescriptor::_clearAndRebuildLUT()
	{
		std::for_each(m_vvGridAllFeatures.begin(), m_vvGridAllFeatures.end(),
		              [](std::vector<FeaturePoint*>& v) { v.clear(); });
		std::for_each(m_vvGridTrackedFeature.begin(), m_vvGridTrackedFeature.end(),
		              [](std::vector<TrackedFeature*>& v) { v.clear(); });

		for (int i = 0, n = m_vvGridPoints.size(); i < n; ++i)
		{
			if (m_vvGridPoints[i].empty()) continue;
			auto& neighborVector = m_vvNeighbor_LUT[i];
			for (auto feature_point : m_vvGridPoints[i])
			{
				for (auto& Index : neighborVector)
					m_vvGridAllFeatures[Index].push_back(feature_point);
			}
		}
	}

	void FeatureTrackerDescriptor::_matchFeatures(std::vector<TrackedFeaturePtr>& goodTracks,
	                                  std::vector<TrackedFeaturePtr>& badTracks)
	{
		static CamModel* camModel = CamModel::getCamModel();

		dR = frame1->state->m_Rwi.transpose() * frame0->state->m_Rwi;
		for (auto tracked_feature : *m_pvTrackedFeatures)
		{
			if (tracked_feature->m_bDead) continue;

			int GridIdx;
			int* pDesc;
			if (tracked_feature->m_pState)
			{
				Vector3f pt = frame1->state->m_Rwi.transpose() * (tracked_feature->m_pState->m_Pw - frame1->state->m_Pwi);
				

				Vector2f px = camModel->imuToImage(pt);
				if (!camModel->inView(px))
				{
					tracked_feature->m_bDead = true;
					continue;
				}

				tracked_feature->m_PredictedPx = px;

				GridIdx = int(px.x() / m_nStepX) + int(px.y() / m_nStepY) * m_nXGrid;
				pDesc = tracked_feature->m_desc.ptr<int>();
			}
			else
			{
				auto& lastVisualOb = tracked_feature->m_vVisualObs.back();
				Vector3f ray = dR * lastVisualOb.m_Ray;

				static Matrix3f Rci = camModel->getRci();
				Vector2f px = camModel->camToImage(Rci * ray);
				if (!camModel->inView(px))
				{
					tracked_feature->m_bDead = true;
					continue;
				}


				tracked_feature->m_PredictedPx = px;
				GridIdx = int(px.x() / m_nStepX) + int(px.y() / m_nStepY) * m_nXGrid;
				pDesc = tracked_feature->m_desc.ptr<int>();
			}

			int descDistThresh = 1.5 * tracked_feature->m_MeanDescDist;


			FeaturePoint* bestMatch = nullptr;
			int minDescDist = std::numeric_limits<int>::max();
			m_DistThresh2 = std::max(36.f, std::min(tracked_feature->m_LastMovedPx * 2, 81.f));

			for (auto feature_point : m_vvGridAllFeatures[GridIdx])
			{
				if (feature_point->m_pTrackedFeature) continue;
				if (!feature_point->m_bIsORB) continue;

				int descDist = HammingDistance(pDesc, feature_point->m_desc.ptr<int>());
				if (descDist < minDescDist)
				{
					bestMatch = feature_point;
					minDescDist = descDist;
				}
			}


			if (minDescDist < descDistThresh)
			{
				tracked_feature->m_LastDescDist = minDescDist;
				tracked_feature->m_pMatchedFeature = bestMatch;
				badTracks.push_back(tracked_feature);
				continue;
			}
			tracked_feature->m_nMatchFailed++;
			tracked_feature->m_pMatchedFeature = nullptr;
			if (tracked_feature->m_nMatchFailed > 3)
			{
				tracked_feature->m_bDead = true;
				continue;
			}
			tracked_feature->m_bDead = true;
			goodTracks.push_back(tracked_feature);
		}
	}

	void FeatureTrackerDescriptor::_SSDMatch(std::vector<TrackedFeaturePtr>& goodTracks)
	{
		static CamModel* camModel = CamModel::getCamModel();
		for (auto goodTrack : goodTracks)
		{
			if (!goodTrack->m_bDead) continue;
			auto& lastVisualOb = goodTrack->m_vVisualObs.back();
			ZMSSDPatch<8> patch0(img0, (lastVisualOb.m_px.x() - 0.5) * 0.5, (lastVisualOb.m_px.y() - 0.5) * 0.5);
			FeaturePoint* bestMatch = nullptr;
			int ssdThresh = 6400;
			int bestSSD = std::numeric_limits<int>::max();
			

			Vector3f ray = dR * lastVisualOb.m_Ray;

			static Matrix3f Rci = camModel->getRci();
			Vector2f px = camModel->camToImage(Rci * ray);
			if (!camModel->inView(px))
			{
				goodTrack->m_bDead = true;
				continue;
			}

			goodTrack->m_PredictedPx = px;

			int grid_idx = int(px.x() / m_nStepX) + int( px.y() / m_nStepY) * m_nXGrid;
			for (auto feature_point : m_vvGridAllFeatures[grid_idx])
			{
				if (feature_point->m_bIsORB || feature_point->m_pTrackedFeature) continue;
				if ((feature_point->px - goodTrack->m_PredictedPx).squaredNorm() > m_DistThresh2) continue;
				int ssd = patch0.computeZMSSD(img1, feature_point->m_corner.pt.x, feature_point->m_corner.pt.y);
				if (ssd < bestSSD)
				{
					bestMatch = feature_point;
					bestSSD = ssd;
				}
			}

			if (bestSSD < ssdThresh)
			{
				bestMatch->m_pTrackedFeature = goodTrack.get();
				goodTrack->addVisualObservation(bestMatch, frame1);
				m_vvGridTrackedFeature[goodTrack->m_GridIdx].push_back(goodTrack.get());
				goodTrack->m_bDead = false;
				goodTrack->m_pMatchedFeature = bestMatch;
			}
			else
			{
				goodTrack->m_bDead = true;
				goodTrack->m_pMatchedFeature = bestMatch;
			}
		}
	}

	void FeatureTrackerDescriptor::_do2PointRansac()
	{
		std::vector<Vector3f> ray0, ray1;
		std::vector<TrackedFeature*> goodTracks;
		ray0.reserve(600);
		ray1.reserve(600);
		goodTracks.reserve(600);
		for (const auto& tracked_feature : *m_pvTrackedFeatures)
		{
			if (tracked_feature->m_bDead) continue;
			int nObs = tracked_feature->m_vVisualObs.size();
			auto ray = tracked_feature->m_vVisualObs[nObs - 1].m_Ray;
			ray *= (1 / ray.z());
			ray1.push_back(ray);
			ray = tracked_feature->m_vVisualObs[nObs - 2].m_Ray;
			ray *= (1 / ray.z());
			ray0.push_back(ray);
			goodTracks.push_back(tracked_feature.get());
		}

		std::vector<bool> vInliers;
		m_pTwoPointRansac->findInliers(ray0, ray1, dR, vInliers);
		for (int i = 0, n = vInliers.size(); i < n; ++i)
		{
			if (!vInliers[i])
			{
				auto& track = goodTracks[i];

				track->popObservation();
				track->m_bDead = true;
			}
		}
	}

	void FeatureTrackerDescriptor::_trackNewFrame()
	{
		std::vector<TrackedFeaturePtr> goodTracks;
		std::vector<TrackedFeaturePtr> badTracks;

		_clearAndRebuildLUT();

		_matchFeatures(goodTracks, badTracks);

		_Median_Filter(badTracks, goodTracks, false);

		_SSDMatch(goodTracks);

		DataAssociation::removeOutlierBy2PointRansac(dR, *m_pvTrackedFeatures);


		_detectMoreORBFeatures();
	}


	void FeatureTrackerDescriptor::_Median_Filter(std::vector<TrackedFeaturePtr>& goodTracks,
	                                  std::vector<TrackedFeaturePtr>& badTracks,
	                                  bool verbose)
	{
		if (goodTracks.empty())
			return;

		int nGoods = goodTracks.size();
		std::vector<float> vReprojErrs(nGoods);

		for (int i = 0; i < nGoods; ++i)
		{
			auto& ftTrack = goodTracks[i];
			vReprojErrs[i] = (ftTrack->m_PredictedPx - ftTrack->m_pMatchedFeature->px).squaredNorm();
		}


		auto temp = vReprojErrs;
		auto halfNum = nGoods/ 2;
		nth_element(temp.begin(), temp.begin() + halfNum, temp.end());
		float median = temp[halfNum];
		m_DistThresh2 = std::max(median * 2, 9.f);

		for (int i = 0; i < nGoods; ++i)
		{
			auto tracked_feature = goodTracks[i];
			auto bestMatch = tracked_feature->m_pMatchedFeature;
			if (vReprojErrs[i] < m_DistThresh2)
			{
				int nObs = tracked_feature->m_vVisualObs.size();
				tracked_feature->m_MeanDescDist =
					(tracked_feature->m_MeanDescDist * nObs + tracked_feature->m_LastDescDist) / (nObs + 1);
				if (tracked_feature->m_MeanDescDist < 60) tracked_feature->m_MeanDescDist = 60;
				tracked_feature->m_bDead = false;
				bestMatch->m_pTrackedFeature = tracked_feature.get();
				tracked_feature->addVisualObservation(bestMatch, frame1);
				m_vvGridTrackedFeature[tracked_feature->m_GridIdx].push_back(tracked_feature.get());
				tracked_feature->m_nMatchFailed = 0;
			}
			else
			{
				tracked_feature->m_bDead = true;
				badTracks.push_back(tracked_feature);
			}
		}
	}

	void FeatureTrackerDescriptor::_detect_fast(Mat& image, std::vector<fast::fast_xy>& pts,
	                                  int detection_threshold)
	{
		auto image0 = (fast::fast_byte *)(image.ptr<uchar>(EDGE_THRESHOLD) + EDGE_THRESHOLD);
		fast_corner_detect_10(
			image0, image.cols - EDGE_THRESHOLD * 2,
			image.rows - 2 * EDGE_THRESHOLD, image.step1(), detection_threshold, pts);

		for (auto& xy : pts)
		{
			xy.x += EDGE_THRESHOLD;
			xy.y += EDGE_THRESHOLD;
		}
	}

	int FeatureTrackerDescriptor::_detectMoreORBFeatures()
	{
		int nNewPoints = 0;
		for (int i = 0, n = m_nXGrid * m_nYGrid; i < n; ++i)
		{
			if (m_vvGridTrackedFeature[i].size() < 4 && !m_vvGridPoints[i].empty())
			{
				int nPointsNeeded = 4 - m_vvGridTrackedFeature[i].size();
				for (auto feature_point : m_vvGridPoints[i])
				{
					if (!nPointsNeeded) break;
					if (!feature_point->m_bIsORB||feature_point->m_pTrackedFeature) continue;
					TrackedFeaturePtr trackedFeature = std::make_shared<TrackedFeature>();

					feature_point->m_pTrackedFeature = trackedFeature.get();
					trackedFeature->addVisualObservation(feature_point, frame1);
					trackedFeature->m_desc = feature_point->m_desc;
					--nPointsNeeded;
					nNewPoints++;
					m_pvTrackedFeatures->push_back(trackedFeature);
				}
			}
		}
		return nNewPoints;
	}
}
