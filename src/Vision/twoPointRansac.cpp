/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include "twoPointRansac.h"
#include <CamModel/CamModel.h>


namespace DeltaVIO{
	TwoPointRansac::TwoPointRansac(float maxReprojErr,int maxIterNum)
	{
		m_iMaxIterNum = maxIterNum;
		m_fmaxSqrReprojErr = maxReprojErr*maxReprojErr;
	}
	
	void TwoPointRansac::computeEssentialMatrix()
	{

		auto& ray0_0 = (*m_vpRay0)[m_iSample0];
		auto& ray10_0 = m_vRay10[m_iSample0];
		auto& ray0_1 = (*m_vpRay0)[m_iSample1];
		auto& ray10_1 = m_vRay10[m_iSample1];

		float ray0_0_x = ray0_0.x(), ray0_0_y = ray0_0.y(), ray0_0_z = ray0_0.z();
		float ray10_0_x = ray10_0.x(), ray10_0_y = ray10_0.y(), ray10_0_z = ray10_0.z();
		float ray0_1_x = ray0_1.x(), ray0_1_y = ray0_1.y(), ray0_1_z = ray0_1.z();
		float ray10_1_x = ray10_1.x(), ray10_1_y = ray10_1.y(), ray10_1_z = ray10_1.z();

		//formulation (10)
		float c1 = -ray0_1_x*ray10_1_z + ray10_1_x*ray0_1_z;
		float c2 = ray10_0_x*ray0_0_y - ray0_0_x*ray10_0_y;
		float c3 = -ray0_0_x*ray10_0_z + ray10_0_x*ray0_0_z;
		float c4 = -ray0_0_y*ray10_0_z + ray10_0_y*ray0_0_z;
		float c5 = ray10_1_x*ray0_1_y - ray0_1_x*ray10_1_y;
		float c6 = -ray0_1_y*ray10_1_z + ray10_1_y*ray0_1_z;

		//formulation (9)
		float alpha = -atan2(c5*c4 - c2*c6, c5*c3 - c2*c1);
		float beta = -atan2(c2, c4*cos(alpha) + c3*sin(alpha));	
		//formulation (7)
		m_mE <<		0,				-cos(beta),				-sin(beta)*sin(alpha),
					cos(beta),			0,					-sin(beta)*cos(alpha),
			sin(beta)*sin(alpha),	sin(beta)*cos(alpha),			 0;
	}

	int TwoPointRansac::selectInliers(std::vector<bool>& inliers)
	{
		static float maxSqrErr =m_fmaxSqrReprojErr; 
		int nInliers = 0;
		for (int i = 0; i < m_nPoints; ++i)
		{
			Vector3f FtRay0 = m_mE.transpose() * (*m_vpRay0)[i];
			float a0 = m_vRay10[i].dot(FtRay0);
			float s0 = 1.f/((*m_vpRay0)[i].tail<1>()* m_vRay10[i].tail<1>());

			Vector3f Fray0 = m_mE*m_vRay10[i];
			float a1 = (*m_vpRay0)[i].dot(Fray0);


			m_vReprojErr[i] = std::max(a0*a0* s0, a1*a1* s0);
			
			if (m_vReprojErr[i] < maxSqrErr)
			{
				inliers[i] = true;
				nInliers++;
			}
			else
				inliers[i] = false;

		}
		return nInliers;
	}
	int TwoPointRansac::findInliers(const std::vector<Vector3f>& ray0, const std::vector<Vector3f>& ray1, const Matrix3f&dR, std::vector<bool>& inliers)
	{
		m_nPoints = ray0.size();
		if (!m_nPoints) return 0;
		m_mdR = dR.transpose();
		m_vpRay0 = &ray0; m_vpRay1 = &ray1;
		m_vRay10.resize(m_nPoints);
		m_vReprojErr.resize(m_nPoints);
		m_sampleIdx = 0;


		std::vector<bool> vInliers(m_nPoints);

		int nMaxInliers = -1;
		for (int i = 0; i < m_nPoints; ++i)
		{
			m_vRay10[i] = m_mdR * ray1[i];
		}

		for (int it = 0, n = m_iMaxIterNum; it < n; ++it)
		{
			if (!NextSample()) break;
			computeEssentialMatrix();
			const int nInliers = selectInliers(vInliers);
			if (nInliers > nMaxInliers)
			{
				inliers = vInliers;
				nMaxInliers = nInliers;
				n = updateIterNum(nInliers);
			}
		}
		return nMaxInliers;
	}





int TwoPointRansac::updateIterNum(int nInliers)
{
	return log(1 - m_confidence) / log(1 - pow(1 - double(m_nPoints - nInliers) / m_nPoints, 2));
}

bool TwoPointRansac::NextSample()
{
	if (m_sampleIdx >= m_nSamples) return false;
	m_iSample0 = rand_list[m_sampleIdx*2] % m_nPoints;
	m_iSample1 = rand_list[m_sampleIdx*2+1] % m_nPoints;
	m_sampleIdx++;
	return true;
}
}
