#include "precompile.h"
#include "Vision/FeatureTrackerOpticalFlow.h"
#include "CamModel/CamModel.h"
#include "twoPointRansac.h"
#include "fast/fast.h"
#include "MsckfDataAssociation.h"

namespace DeltaVIO
{
	FeatureTrackerOpticalFlow::FeatureTrackerOpticalFlow(int nMax2Track,int nMaskSize):FeatureTracker(),m_nMax2Track(nMax2Track),m_nMaskSize(nMaskSize)
	{
		assert(nMaskSize % 2);
	}

	inline void FeatureTrackerOpticalFlow::_setMask(int x, int y)
	{
		const int imgStride = CamModel::getCamModel()->width();
		const int halfMaskSize = (m_nMaskSize - 1) / 2;

		if (x < halfMaskSize) x = halfMaskSize;
		if (y < halfMaskSize) y = halfMaskSize;
		if (x >= imgStride - halfMaskSize) x = imgStride - halfMaskSize - 1;
		if (y >= imgStride - halfMaskSize) y = imgStride - halfMaskSize - 1;


		unsigned char* pMask0 = m_pMask + x + (y - halfMaskSize) * imgStride - halfMaskSize;
		unsigned char* pMask1 = pMask0 + imgStride * m_nMaskSize;
		for (; pMask0 < pMask1; pMask0 += imgStride) {
			memset(pMask0, 0, m_nMaskSize);
		}

	}

	void FeatureTrackerOpticalFlow::_extractMorePoints(std::list<TrackedFeaturePtr>& vTrackedFeatures)
	{
		//ReSet Mask Pattern
		memset(m_pMask, 0x01, m_nMaskBufferSize);


		//Set Mask Pattern
		const int imgStride = CamModel::getCamModel()->width();
		
		for (auto tf:vTrackedFeatures)
		{
			if(!tf->m_bDead)
			{
				auto xy = tf->m_vVisualObs.back().m_px;
				_setMask(xy.x(), xy.y());

			}
		}
		//Todo: test mask correctness


		//Extract More Points out of mask

		std::vector<fast::fast_xy> vXys;
		std::vector<int> vScores, vNms;;
		std::vector<std::pair<int, fast::fast_xy>> vTemp;
		int halfMaskSize = (m_nMaskSize - 1) / 2;
		fast::fast_corner_detect_10_mask(m_image.data+ halfMaskSize+ halfMaskSize*imgStride, m_pMask + halfMaskSize + halfMaskSize * imgStride, m_image.cols-m_nMaskSize+1, m_image.rows-m_nMaskSize+1, m_image.step1(), 15, vXys);
		fast::fast_corner_score_10(m_image.data + halfMaskSize + halfMaskSize * imgStride, m_image.step, vXys, 15, vScores);
		fast::fast_nonmax_3x3(vXys, vScores, vNms);

		vTemp.reserve(vXys.size());
		for (auto& idx : vNms) {
			vTemp.emplace_back(vScores[idx], vXys[idx]);
		}

#if USE_STABLE_SORT
		std::stable_sort(vTemp.begin(), vTemp.end(), [](auto& a, auto& b) {return a.first > b.first; });
#else

		std::sort(vTemp.begin(), vTemp.end(), [](auto& a, auto& b) {return a.first > b.first; });
#endif

		for(int i=0,j = m_nMax2Track - m_nTracked;i<vTemp.size()&&j>0;++i)
		{
			int x = vTemp[i].second.x+halfMaskSize;
			int y = vTemp[i].second.y+halfMaskSize;
			if(m_pMask[x+y*imgStride])
			{
				auto fp = m_pFeaturePool+m_iFeature;
				_setMask(x, y);
				j--;
				fp->px.x() = x;
				fp->px.y() = y;
				auto tf = std::make_shared<TrackedFeature>();
				tf->addVisualObservation(fp, m_pCamState);
				vTrackedFeatures.push_back(tf);
				++m_iFeature;
			}
		}
		
	}

	void FeatureTrackerOpticalFlow::_trackPoints(std::list<TrackedFeaturePtr>& vTrackedFeatures)
	{
		if (lastImage.empty())
			return;
		Matrix3f dR = m_pCamState->state->m_Rwi.transpose() * m_pCamState0->state->m_Rwi;

		auto lkOpticalFlow =  cv::SparsePyrLKOpticalFlow::create();
		std::vector<cv::Point2f> pre, now;
		std::vector<unsigned char> status;
		for (auto tf:vTrackedFeatures)
		{
			if(!tf->m_bDead)
				pre.emplace_back(tf->m_vVisualObs.back().m_px.x(), tf->m_vVisualObs.back().m_px.y());
		}
		now = pre;
		lkOpticalFlow->calc(lastImage, m_image, pre, now, status);

		int ii = 0;
		auto camModel = CamModel::getCamModel();
		for(auto it= vTrackedFeatures.begin();it!=vTrackedFeatures.end();++it,++ii)
		{
			if ((*it)->m_bDead) continue;
			if (status[ii])
			{
				auto fp = m_pFeaturePool + m_iFeature;
				fp->px.x() = now[ii].x;
				fp->px.y() = now[ii].y;
				if(camModel->inView(fp->px))
				{
					m_iFeature++;
					m_nTracked++;
					(*it)->addVisualObservation(fp, m_pCamState);
					continue;;
				}
			
			}
			
			(*it)->m_bDead = true;
			
		}


		DataAssociation::removeOutlierBy2PointRansac(dR, vTrackedFeatures);
		
		

	}

	void FeatureTrackerOpticalFlow::matchNewFrame(std::list<TrackedFeaturePtr>& vTrackedFeatures, cv::Mat& image,
		Frame* camState)
	{
		m_iFeature = 0;
		m_image = image;
		m_pCamState0 = m_pCamState;
		m_pCamState = camState;
		
		//Set cnt for tracked points to zero
		m_nTracked = 0;
		
		//Init mask buffer
		if(m_pMask == nullptr)
		{
			m_nMaskBufferSize = CamModel::getCamModel()->area();
			m_pMask = new unsigned char[m_nMaskBufferSize];
		}

		_trackPoints(vTrackedFeatures);
		
		// Extract more points if there are more points can be tracked.
		if(m_nTracked<m_nMax2Track)
		{
			_extractMorePoints(vTrackedFeatures);
		}

		
		lastImage = m_image;
		
		
	}

	FeatureTrackerOpticalFlow::~FeatureTrackerOpticalFlow()
	{
		if (m_pMask)
			delete[] m_pMask;
	}
}
