/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include "Config.h"

using namespace Eigen;

namespace DeltaVIO
{
	int Configuration::DatasetType;
	string Configuration::DatasetPath;
	int Configuration::SerialRun;
	int Configuration::ImageStartNumber;

	int Configuration::nImuSample;
	FileStorage Configuration::m_configFile;
	float Configuration::ImageNoise2;
	float Configuration::GyroNoise2;
	float Configuration::AccNoise2;
	float Configuration::GyroBiasNoise2;
	float Configuration::AccBiasNoise2;

	string Configuration::CameraCalibFile;

	void Configuration::readConfigFile()
	{
		_clear();

		m_configFile.open("Config/Config.yaml", FileStorage::READ);
		if (!m_configFile.isOpened())
		{
			throw std::runtime_error("fail to open config file");
		}

		string temp;
		m_configFile["DatasetType"] >> temp;
		if (temp == "EUROC")
			DatasetType = EUROC;
		else
			throw std::runtime_error("Unknown DataSource:"+temp);
		
		m_configFile["DatasetPath"] >> DatasetPath;


		m_configFile["ImuSampleFps"] >> nImuSample;
		if (nImuSample == 0)
		{
			throw std::runtime_error("Unable to load IMU sample speed.");
		}

		m_configFile["PixelNoise"] >> ImageNoise2;
		m_configFile["GyroNoise"] >> GyroNoise2;
		m_configFile["AccNoise"] >> AccNoise2;
		m_configFile["GyroBiasNoise"] >> GyroBiasNoise2;
		m_configFile["AccBiasNoise"] >> AccBiasNoise2;
		GyroNoise2 = GyroNoise2 *(GyroNoise2* nImuSample);
		AccNoise2 = AccNoise2 * (AccNoise2*nImuSample);
		GyroBiasNoise2 = GyroBiasNoise2 *(GyroBiasNoise2* nImuSample);
		AccBiasNoise2 = AccBiasNoise2 *(AccBiasNoise2*nImuSample);
		ImageNoise2 = ImageNoise2 * ImageNoise2;
		m_configFile["CameraCalibrationPath"] >> CameraCalibFile;
		m_configFile["ImageStartIdx"] >> ImageStartNumber;
		m_configFile["SerialRun"] >> SerialRun;

	}

	void Configuration::generateTemplateFile()
	{
		cv::FileStorage configFile("Config/Config.yaml",FileStorage::WRITE);
		configFile.write("PixelNoise",2.);
		configFile.write("GyroNoise",2.);
		configFile.write("AccNoise",2.);
		configFile.write("GyroBiasNoise",2.);
		configFile.write("AccBiasNoise",2.);
		configFile.write("ImageStartIdx",2);
		configFile.write("SerialRun",1);
		configFile.write("CameraCalibrationPath","path");
		configFile.write("ImuSampleFps","400");
		configFile.write("DatasetPath","Path");
		configFile.write("DatasetType","Type");
		
	}

	void Configuration::_clear()
	{
		DatasetType = 0;
		SerialRun = 0;
		ImageStartNumber = 0;
	}

}
