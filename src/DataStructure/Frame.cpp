/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include <DataStructure/Frame.h>
#include <DataStructure/TrackedFeature.h>
namespace DeltaVIO
{
	Frame::Frame()
	{
		state = new CamState();
		state->m_pHost = this;
	}

	Frame::~Frame()
	{
		if (state) delete state;
	}

	void Frame::removeLinksFromAllTrackedFeatures(TrackedFeature* ftTrack)
	{
		 m_vTrackedFeatures.erase(ftTrack);
	}

	void Frame::removeAllFeatures()
	{
		for(auto feature:m_vTrackedFeatures){
			for(auto&observation:feature->m_vVisualObs){
				if (observation.m_linkFrame == this) {
					int nSize = feature->m_vVisualObs.size();
                    if (!feature->m_bDead) 
                    {
                        observation = feature->m_vVisualObs[nSize - 2];
                        feature->m_vVisualObs[nSize - 2] = feature->m_vVisualObs[nSize - 1];
                    }
                    else 
                        observation = feature->m_vVisualObs.back();              
                    feature->m_vVisualObs.pop_back();
					break;
				}
			}
		}
		m_vTrackedFeatures.clear();
	}
}
