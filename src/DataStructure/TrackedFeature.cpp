/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include "CamModel/CamModel.h"
#include "FeatureTrackerDescriptor.h"
#include <DataStructure/TrackedFeature.h>

namespace DeltaVIO
{

	bool TrackedFeature::triangulate()
	{
		if (m_pState) return true;

		clear();

		auto& leftVisualOb = m_vVisualObs.front();
		z = leftVisualOb.m_Ray;
		z /= z[2];
		z[2] = 1 / 2.f;

		static Vector3f Pic = CamModel::getCamModel()->getPic();
		static Matrix3f Rci = CamModel::getCamModel()->getRci();
		const int nSize = m_vVisualObs.size() - 1;
		m_vdR.resize(nSize);
		m_vdt.resize(nSize);
		Matrix3f leftR = leftVisualOb.m_linkFrame->state->m_Rwi;
		Vector3f leftP = leftVisualOb.m_linkFrame->state->m_Pwi;
		{
			for (int i = 0; i < nSize; ++i)
			{
				auto& ob = m_vVisualObs[i + 1];
				Matrix3f R_i = ob.m_linkFrame->state->m_Rwi;
				Vector3f P_i = ob.m_linkFrame->state->m_Pwi;

				m_vdR[i] = Rci * R_i.transpose() * leftR;
				m_vdt[i] = Rci * R_i.transpose() * (leftP - P_i) - Rci * Pic;

			}
		}

		solve();
		
		if (m_pState == nullptr) {
			m_pState = new PointState();
			m_pState->host = this;
		}
		Vector3f cpt = z / z[2];
		cpt[2] = 1.0f / z[2];
		m_pState->m_Pw = leftR * cpt + leftP;
		m_pState->m_Pw_FEJ = m_pState->m_Pw;


		m_vdR.clear();
		m_vdt.clear();
		return m_Result.bConverged;
	}


	float TrackedFeature::evaluateF(bool bNewZ)
    {
		float cost = 0.f;
		const int nSize = m_vVisualObs.size() - 1;
		Matrix23f J23;
        Matrix3f J33;
		Vector3f position;
		
		if(bNewZ)
		{
			position = zNew / zNew[2];
			position[2] = 1.0 / zNew[2];
		}
		else {
			position = z / z[2];
			position[2] = 1.0 / z[2];

		}
		HTemp.setZero();
		bTemp.setZero();
        J33 <<	position[2], 0,				-position[0] * position[2],
				0,			position[2],	-position[1] * position[2],
				0,			 0,				-position[2] * position[2];
		for (int i = 0; i < nSize; ++i)
		{
			Vector3f p_cam = m_vdR[i] * position + m_vdt[i];

			Vector2f px = CamModel::getCamModel()->camToImage(p_cam, J23);


			Vector2f r = m_vVisualObs[i + 1].m_px - px;
			cost += r.squaredNorm();

			Matrix23f J = J23 * m_vdR[i] * J33;

			HTemp.noalias() += J.transpose() * J;
			bTemp.noalias() += J.transpose() * r;

		}
		//if(!bNewZ)
		//	H += Matrix3f::Identity()*FLT_EPSILON;
		return cost;
    }

	bool TrackedFeature::userDefinedDecentFail()
	{
		return zNew[2] < 0;
	}


	void TrackedFeature::addVisualObservation(FeaturePoint* featurePoint, Frame* frame)
	{

		m_bDead = false;
		m_GridIdx = featurePoint->m_GridIdx;

		if(!m_vVisualObs.empty()){
			m_LastMovedPx = (m_vVisualObs.back().m_px - featurePoint->px).squaredNorm();
		}
		m_nObs++;

		Vector3f ray0 = CamModel::getCamModel()->imageToImu(featurePoint->px);

		m_vVisualObs.emplace_back(featurePoint->px, ray0, frame);

        auto &rightOb = m_vVisualObs.back();


		frame->m_vTrackedFeatures.insert(this);
#if !TEST_OPTICAL_FLOW
		m_rayAngle0 = m_rayAngle;
        Vector3f ray1 = rightOb.m_linkFrame->state->m_Rwi*rightOb.m_Ray;
		
        float minDot = 2;
        for (size_t i = 0, length = m_vVisualObs.size() - 1; i < length; i++)
        {
            auto&visualOb = m_vVisualObs[i];
            float dot = (visualOb.m_linkFrame->state->m_Rwi*visualOb.m_Ray).dot(ray1);
            if (dot < minDot)
                minDot = dot;
        }
        if (minDot > 0 && minDot < 1) {
            m_rayAngle = std::max(m_rayAngle, acos(minDot));
        }

#endif

	}

	void TrackedFeature::drawFeatureTrack(cv::Mat& image, cv::Scalar color)const
	{
		for (int i=0;i<m_vVisualObs.size()-1;++i)
		{
			if((m_vVisualObs[i].m_px - m_vVisualObs[i+1].m_px).squaredNorm()>900)
				continue;
			cv::line(image,cv::Point(m_vVisualObs[i].m_px.x(),m_vVisualObs[i].m_px.y()),cv::Point(m_vVisualObs[i+1].m_px.x(),m_vVisualObs[i+1].m_px.y()),_GREEN_SCALAR,1);
			cv::circle(image,cv::Point(m_vVisualObs[i].m_px.x(),m_vVisualObs[i].m_px.y()),2, color);
		}
		cv::circle(image,cv::Point(m_vVisualObs.back().m_px.x(),m_vVisualObs.back().m_px.y()),2, color);
	}

	bool TrackedFeature::userDefinedConvergeCriteria()
	{
		return m_Result.cost < 10;
	}

	void TrackedFeature::popObservation()
	{
		m_vVisualObs.back().m_linkFrame->removeLinksFromAllTrackedFeatures(this);
		m_rayAngle = m_rayAngle0;
		m_vVisualObs.pop_back();
	}

	void TrackedFeature::removeLinksInCamStates(){
		for (auto&ob : m_vVisualObs)
			ob.m_linkFrame->removeLinksFromAllTrackedFeatures(this);
		m_vVisualObs.clear();
	}






}
