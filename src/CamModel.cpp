/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include <Config.h>
#include <CamModel/CamModel.h>
#include <CamModel/PinholeModel.h>
#include <CamModel/RadTanModel.h>


namespace DeltaVIO
{
	CamModel* CamModel::camModel = nullptr;

	void CamModel::loadCalibrations()
	{
		FileStorage config(Configuration::CameraCalibFile + "/calibrations.yaml", FileStorage::READ);

		std::string type;
		config["CamType"] >> type;

		if (type == "Pinhole")
		{
			camModel = PinholeModel::createFromConfig(config);
		}
		else if (type == "RadTan")
		{
			camModel = RadTanModel::createFromConfig(config);
		}


		//Transformation Matrix from imu frame to camera frame
		Mat Tci;

		config["Tci"] >> Tci;

		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				camModel->m_Rci(i, j) = Tci.at<double>(i, j);
				camModel->m_Rci(i, j) = Tci.at<double>(i, j);
			}
			camModel->m_Pic(i) = Tci.at<double>(i, 3);
		}

		camModel->m_Pic = -camModel->m_Rci.transpose() * camModel->m_Pic;
	}

	void CamModel::generateCalibrations(const std::string& path)
	{
		FileStorage config(Configuration::CameraCalibFile + "/calibrations.yaml", FileStorage::WRITE);
		config.writeComment("Camera Type: Pinhole or RadTan");
		config.write("CamType","Pinhole");
		config.writeComment("Tci: Transformation Matrix from imu frame to camera frame");
		cv::Mat Tci(3,4,CV_64F);
		setIdentity(Tci);
		config.write("Tci",Tci);
	}
}
