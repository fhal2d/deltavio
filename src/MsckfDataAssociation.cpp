#include "precompile.h"
#include "MsckfDataAssociation.h"
#include "CamModel/CamModel.h"


namespace DeltaVIO{
	namespace DataAssociation
	{
		
		TwoPointRansac* m_pTwoPointRansac = nullptr;
		SquareRootEKFSolver* m_pSquareRootSolver = nullptr;
		std::vector<TrackedFeaturePtr> m_vTrackedFeatureToUpdate;
		std::vector<TrackedFeaturePtr> m_vTrackedFeatureNextUpdate;
		std::vector<vector<TrackedFeaturePtr>> m_Grid22;
		

		void initDataAssociation(SquareRootEKFSolver* solver)
		{
			if (!m_pTwoPointRansac)
				m_pTwoPointRansac = new TwoPointRansac();
			m_pSquareRootSolver = solver;
			m_Grid22.resize(4);
		}

		void removeOutlierBy2PointRansac(Matrix3f& dR, std::list<TrackedFeaturePtr>& vTrackedFeatures)
		{
			assert(m_pTwoPointRansac);
			
			std::vector<Vector3f> ray0, ray1;
			std::vector<TrackedFeature*> goodTracks;
			ray0.reserve(600);
			ray1.reserve(600);
			goodTracks.reserve(600);

			for (const auto& tracked_feature : vTrackedFeatures)
			{
				if (tracked_feature->m_bDead) continue;
				int nObs = tracked_feature->m_vVisualObs.size();
				auto ray = tracked_feature->m_vVisualObs[nObs - 1].m_Ray;
				ray *= (1 / ray.z());
				ray1.push_back(ray);
				ray = tracked_feature->m_vVisualObs[nObs - 2].m_Ray;
				ray *= (1 / ray.z());
				ray0.push_back(ray);
				goodTracks.push_back(tracked_feature.get());
			}

			std::vector<bool> vInliers;
			m_pTwoPointRansac->findInliers(ray0, ray1, dR, vInliers);
			for (int i = 0, n = vInliers.size(); i < n; ++i)
			{
				if (!vInliers[i])
				{
					auto& track = goodTracks[i];

					track->popObservation();
					track->m_bDead = true;
				}
			}
		}



		
		void _addBufferPoints(vector<std::shared_ptr<TrackedFeature>>& vDeadFeature)
		{
			constexpr int MAX_BUFFER_OBS = 5;
			for (auto tracked_feature : m_vTrackedFeatureNextUpdate)
			{
				if (tracked_feature->m_vVisualObs.size() > MAX_BUFFER_OBS)
					vDeadFeature.push_back(tracked_feature);
				else
					tracked_feature->removeLinksInCamStates();
			}
			m_vTrackedFeatureNextUpdate.clear();
		}

		void _addDeadPoints(std::list<TrackedFeaturePtr>& vTrackedFeatures,vector<std::shared_ptr<TrackedFeature>>& vDeadFeature)
		{
			constexpr int MAX_OBS = 10;
			for (auto iter = vTrackedFeatures.begin(); iter != vTrackedFeatures.end();) {
				auto tracked_feature = *iter;

				if (tracked_feature->m_bDead) {
					if (tracked_feature->m_nObs >= MAX_OBS)
						vDeadFeature.push_back(tracked_feature);
					else
						tracked_feature->removeLinksInCamStates();
					iter = vTrackedFeatures.erase(iter);
					continue;
				}
				if (tracked_feature->m_vVisualObs.size() >= MAX_OBS)
					vDeadFeature.push_back(tracked_feature);
				++iter;
			}
		}

		void _pushPoints2Grid(const vector<std::shared_ptr<TrackedFeature>>& vDeadFeature)
		{
			static std::vector<std::vector<TrackedFeaturePtr> > vvGrid44(4 * 4);
			static CamModel* camModel = CamModel::getCamModel();
			static const int STEPX = camModel->width() / 4;
			static const int STEPY = camModel->height() / 4;

			auto comparator_less = [](const TrackedFeaturePtr& a, const TrackedFeaturePtr& b)
			{
				return a->m_bDead == b->m_bDead ? a->m_rayAngle < b->m_rayAngle : a->m_bDead < b->m_bDead;
			};

			auto selectTop2 = [&](const std::vector<TrackedFeaturePtr>& src, std::vector<TrackedFeaturePtr>& dst)
			{
				TrackedFeaturePtr pFirst = nullptr, pSecond = nullptr;
				if (!src.empty()) {

					for (auto tracked_feature : src) {
						if (!pSecond || comparator_less(pSecond, tracked_feature)) {
							if (pSecond && pSecond->m_bDead)
								m_vTrackedFeatureNextUpdate.push_back(pSecond);
							pSecond = tracked_feature;
							if (!pFirst || comparator_less(pFirst, pSecond)) {
								std::swap(pFirst, pSecond);
							}
						}
						else {
							if (tracked_feature->m_bDead)
								m_vTrackedFeatureNextUpdate.push_back(tracked_feature);
						}
					}
					if (pSecond)
						dst.push_back(pSecond);
					if (pFirst && pFirst != pSecond)
						dst.push_back(pFirst);
				}
			};
			for (auto deadFeature : vDeadFeature) {
				auto& ob = deadFeature->m_vVisualObs.back();
				vvGrid44[int(ob.m_px.x() / STEPX) + 4 * int(ob.m_px.y() / STEPY)].push_back(deadFeature);
			}


			static int LUT[16] =
			{ 0,0,1,1,
				0,0,1,1,
				2,2,3,3,
				2,2,3,3 };

			for (int i = 0; i < 16; ++i) {
				selectTop2(vvGrid44[i], m_Grid22[LUT[i]]);
			}

			for (auto& grid : m_Grid22) {
				std::sort(grid.begin(), grid.end(), comparator_less);
			}

			for (auto& grid : vvGrid44)
				grid.clear();
		}



		void _tryAddMsckfPoseConstraint()
		{
			int nPointsPerGrid = MAX_MSCKF_FEATURE / 4;
			int nPointsLeft = MAX_MSCKF_FEATURE;

			auto triangleAndVerify = [&](TrackedFeaturePtr& track)
			{
				if (track->triangulate())
				{
					m_pSquareRootSolver->computeMsckfConstraint(track);

					return m_pSquareRootSolver->MahalanobisTest(track->m_pState);

				}
				return false;
			};
			auto selectPoints = [&]()
			{
				for (auto& grid : m_Grid22)
				{
					int nPointsAdded = 0;
					while (nPointsAdded < nPointsPerGrid && !grid.empty())
					{
						auto& ft = grid.back();
						if (triangleAndVerify(ft))
						{
							++nPointsAdded;
							--nPointsLeft;


							m_pSquareRootSolver->addVisualObservation(ft->m_pState);

							m_vTrackedFeatureToUpdate.push_back(ft);
							ft->m_bDead = true;
						}
						grid.pop_back();
					}
				}
			};
			auto bufferPoints = [&]()
			{
				for (auto& grid : m_Grid22)
				{
					for (auto& ft : grid)
					{
						if (ft->m_bDead)
						{
							m_vTrackedFeatureNextUpdate.push_back(ft);
						}
					}
					grid.clear();
				}
			};

			selectPoints();
			nPointsPerGrid = nPointsLeft / 4;

			if (nPointsPerGrid)
				selectPoints();
			bufferPoints();
		}
		
		void doDataAssociation(std::list<TrackedFeaturePtr>& vTrackedFeatures)
		{

			m_vTrackedFeatureToUpdate.clear();

			static std::vector<TrackedFeaturePtr> vDeadFeature;

			_addBufferPoints(vDeadFeature);

			_addDeadPoints(vTrackedFeatures,vDeadFeature);

			_pushPoints2Grid(vDeadFeature);

			vDeadFeature.clear();

			_tryAddMsckfPoseConstraint();
		}



	}
}
