/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include "utils/Ticktock.h"
#include <unordered_map>


std::unordered_map<std::string,cv::TickMeter> TickTock::m_tickMeters;
 void TickTock::outputResult(std::string fileName)
{
	std::ofstream fout(fileName);
	for (auto&tk:m_tickMeters)
	{
		fout<<tk.first<<":"<<tk.second.getTimeSec()/tk.second.getCounter()<<"s"<< std::endl;
	}
}

 cv::TickMeter& TickTock::getTicker(std::string name)
{
	return m_tickMeters[name];
}