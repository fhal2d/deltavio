/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include <fstream>
#include "utils/utils.h"
#include <dataSet/DataSet_EuRoc.h>

using namespace std;

namespace DeltaVIO
{
	DataSet_EuRoc::DataSet_EuRoc()
	{
		m_iImageIdx = Configuration::ImageStartNumber;
		m_iImuIndex = 0;

		m_dataSetDir = Configuration::DatasetPath;
		m_camDir = m_dataSetDir + "/cam0/";
		m_imuDir = m_dataSetDir + "/imu0/";

		_loadIMU();
		_loadImage();
	}


	DataSet_EuRoc::~DataSet_EuRoc()
	{
		stop();
	}

	bool DataSet_EuRoc::start()
	{
		m_Thread = std::thread(&DataSet_EuRoc::runThread, this);
		return true;
	}

	bool DataSet_EuRoc::stop()
	{
		if(m_Thread.joinable())
			m_Thread.join();
		return true;
	}



	void DataSet_EuRoc::runThread()
	{
		while (m_iImageIdx < m_vImages.size())
		{
			auto& imageInput = m_vImages[m_iImageIdx];
			auto timestamp = imageInput.m_llTimestamp;

			
			while (m_iImuIndex < m_vIMU.size() && m_vIMU[m_iImuIndex].m_llTimestamp < timestamp + 5e7)
			{
				auto& imuInput = m_vIMU[m_iImuIndex++];
				std::lock_guard<std::mutex> lck(m_ImuMutex);
				
				for (auto& imu_listener : m_ImuListener)
				{
					imu_listener->ImuReceived(imuInput.m_data,imuInput.m_llTimestamp*1e-9);
				}
				
			}

			auto img = cv::imread(imageInput.m_imgFilePath, CV_LOAD_IMAGE_GRAYSCALE);
			if (img.empty())
			{
				throw std::runtime_error("Failed to load images");
			}
			int width_crop = 0;
			if(img.cols>640)
				 width_crop = (img.cols - 640) / 2;

			{
				std::lock_guard<std::mutex> lck(m_ImageMutex);
				for (auto& image_listener : m_ImageListener)
				{
					image_listener->ImageReceived(timestamp*1e-9, img.colRange(width_crop, width_crop + 640));
				}
			}
			m_iImageIdx++;

			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}

	}

		void DataSet_EuRoc::_loadIMU()
	{
		ifstream imuCsv;
		imuCsv.open(m_imuDir + "/data.csv");

		ImuData imuData;
		std::string s;
		getline(imuCsv, s);//read first comment line
		while (!imuCsv.eof())
		{
			getline(imuCsv, s);
			if (s.empty()) continue;

			auto nums = split(s, ',');
			imuData.m_llTimestamp = stringToNumber<long long>(nums[0]);
			for (int i = 0; i < 6; i++)
			{
				imuData.m_data[i] = stringToNumber<float>(nums[i+1]);
			}

			auto& data = imuData.m_data;

			data[0] *= -1;
			std::swap(data[0], data[1]);

			data[3] *=-1;
			std::swap(data[3], data[4]);

			m_vIMU.push_back(imuData);
		}
		imuCsv.close();
	}


	void DataSet_EuRoc::_loadImage()
	{
		const string path = m_camDir + "/data.csv";
		ifstream camCsv;
		camCsv.open(path);
		if (!camCsv.is_open())
		{
		   throw std::runtime_error("Failed to open file:"+path);
		}

		ImageData ImageData;
		string s;
		getline(camCsv, s);
		while (!camCsv.eof())
		{
			getline(camCsv, s);
			if (s.empty()) break;

			std::vector<std::string> nums = split(s, ',');
			ImageData.m_llTimestamp = stringToNumber<long long>(nums[0]);
			ImageData.m_imgFilePath = m_camDir + "/data/" + nums[0] + ".png";

			m_vImages.push_back(ImageData);
		}

		camCsv.close();
	}


}
