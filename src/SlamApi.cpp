/**
* This file is part of Delta_VIO.
*
* Delta_VIO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Delta_VIO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Delta_VIO. If not, see <http://www.gnu.org/licenses/>.
*/
#include "precompile.h"
#include "SlamApi.h"
#include "DataSet/DataSet_EuRoc.h"
#include "MsckfVIOObsever.h"
#include "CamModel/CamModel.h"
#include "ImuBuffer.h"
#if ENABLE_VISUALIZER
#include "SlamVisualizer.h"
SlamVisualizer::Ptr slamVisualizer = nullptr;
#endif
using namespace std;
using namespace cv;
using namespace DeltaVIO;

VIOObsever::Ptr vioObsever = nullptr;
DataSet::Ptr dataSource = nullptr;



void initAllModules()
{
	// init msckf algorithm module
	vioObsever = make_shared<MsckfVIOObsever>();

	//Init dataset module
	switch (Configuration::DatasetType)
	{
	case Configuration::EUROC:
		dataSource = std::static_pointer_cast<DataSet>(
			std::make_shared<DataSet_EuRoc>());
		dataSource->addImageListener(vioObsever.get());
		dataSource->addImuListener(&ImuBuffer::getBuffer());
		break;

	default:
		throw std::runtime_error("Dataset Error");
	}
#if ENABLE_VISUALIZER
	slamVisualizer = std::make_shared<SlamVisualizer>(640,480);
	vioObsever->addFrameAdapter(slamVisualizer.get());
	vioObsever->addWorldPointAdapter(slamVisualizer.get());
#endif
}


void startAllModule()
{
	if (vioObsever != nullptr)vioObsever->start();

	if (dataSource != nullptr)
		dataSource->start();
	if(slamVisualizer)
		slamVisualizer->start();
}

void loadSystemConfiguration()
{
	Configuration::readConfigFile();
	CamModel::loadCalibrations();
}

void initAndStartSystem()
{
	loadSystemConfiguration();

	initAllModules();

	startAllModule();
}

void joinAndStopSystem()
{
	if (dataSource)
		dataSource->join();
	if (vioObsever)
		vioObsever->stop();
#if ENABLE_VISUALIZER
	if(slamVisualizer)
		slamVisualizer->join();
#endif
	dataSource->clearListener();
}

